export type ApprovedIcons =
  | 'icon-icon-info-gestante'
  | 'icon-icon-nav-salvar-como'
  | 'icon-icon-nav-web-acess'
  | 'icon-icon-viagem-abrangencia'
  | 'icon-icon-viagem-acessoria-mundial'
  | 'icon-icon-viagem-bagagem'
  | 'icon-icon-viagem-hospedagem'
  | 'icon-icon-viagem-perda'
  | 'icon-icon-viagem-pousar'
  | 'icon-icon-cesta-basica'
  | 'icon-icon-vert-auto'
  | 'icon-icon-vert-capitalizacao'
  | 'icon-icon-vert-dental'
  | 'icon-icon-vert-previdencia'
  | 'icon-icon-vert-residencial'
  | 'icon-icon-vert-saude'
  | 'icon-icon-vert-vida'
  | 'icon-icon-vb-beneficios'
  | 'icon-icon-vb-bercario'
  | 'icon-icon-vb-danca'
  | 'icon-icon-vb-documentarios'
  | 'icon-icon-vb-exposicoes'
  | 'icon-icon-vb-facilidades'
  | 'icon-icon-vb-jazz'
  | 'icon-icon-vb-livro'
  | 'icon-icon-vb-musical'
  | 'icon-icon-vb-recompensas'
  | 'icon-icon-vb-teatro'
  | 'icon-icon-vb-todas'
  | 'icon-icon-seta-baixo-a'
  | 'icon-icon-seta-baixo-b'
  | 'icon-icon-seta-baixo-c'
  | 'icon-icon-seta-cima-a'
  | 'icon-icon-cesta-natalidade'
  | 'icon-icon-seta-cima-c'
  | 'icon-icon-seta-direita-a'
  | 'icon-icon-seta-direita-b'
  | 'icon-icon-seta-direita-c'
  | 'icon-icon-seta-esquerda-a'
  | 'icon-icon-seta-esquerda-b'
  | 'icon-icon-seta-esquerda-c'
  | 'icon-icon-saude-agendar-consulta'
  | 'icon-icon-saude-ambulancia'
  | 'icon-icon-saude-cardiograma'
  | 'icon-icon-saude-dental-cirurgia'
  | 'icon-icon-saude-dental-dentista'
  | 'icon-icon-saude-dental-endodontia'
  | 'icon-icon-saude-dental-odontopediatria'
  | 'icon-icon-saude-dental-pais'
  | 'icon-icon-saude-dental-periodontia'
  | 'icon-icon-saude-dental-prevencao'
  | 'icon-icon-saude-dental-protese'
  | 'icon-icon-saude-dental-radiologia'
  | 'icon-icon-saude-dermatologia'
  | 'icon-icon-saude-diaria-hospitalar'
  | 'icon-icon-saude-homeopatia'
  | 'icon-icon-saude-hospital'
  | 'icon-icon-saude-infantil'
  | 'icon-icon-saude-medico'
  | 'icon-icon-saude-planos-saude'
  | 'icon-icon-saude-primeiros-socorros'
  | 'icon-icon-residencial-antenas'
  | 'icon-icon-residencial-assist'
  | 'icon-icon-residencial-bens'
  | 'icon-icon-residencial-chaveiro'
  | 'icon-icon-residencial-cobertura'
  | 'icon-icon-residencial-descarte'
  | 'icon-icon-residencial-eletricista'
  | 'icon-icon-residencial-encanador'
  | 'icon-icon-residencial-explosao'
  | 'icon-icon-residencial-linha-branca'
  | 'icon-icon-residencial-lixeira'
  | 'icon-icon-residencial-obra'
  | 'icon-icon-residencial-patrimonio'
  | 'icon-icon-residencial-personalizado'
  | 'icon-icon-residencial-provisoria'
  | 'icon-icon-residencial-quadros'
  | 'icon-icon-residencial-raio'
  | 'icon-icon-residencial-reparo'
  | 'icon-icon-residencial-restaurante'
  | 'icon-icon-residencial-telhas'
  | 'icon-icon-residencial-vendaval'
  | 'icon-icon-residencial-vidraceiro'
  | 'icon-icon-residencial-vigilancia'
  | 'icon-icon-redes-facebook'
  | 'icon-icon-redes-google'
  | 'icon-icon-redes-instagram'
  | 'icon-icon-redes-linkedin'
  | 'icon-icon-redes-twitter'
  | 'icon-icon-redes-whatsapp'
  | 'icon-icon-redes-youtube'
  | 'icon-icon-nav-adicionar'
  | 'icon-icon-nav-anexar'
  | 'icon-icon-nav-apresentacao'
  | 'icon-icon-nav-atualizar'
  | 'icon-icon-nav-audio'
  | 'icon-icon-nav-baixar'
  | 'icon-icon-nav-busca'
  | 'icon-icon-nav-cadastrar-senha'
  | 'icon-icon-nav-compartilhar'
  | 'icon-icon-nav-configuracoes'
  | 'icon-icon-nav-conversa'
  | 'icon-icon-nav-desligar'
  | 'icon-icon-nav-editar'
  | 'icon-icon-nav-fechar'
  | 'icon-icon-nav-filme'
  | 'icon-icon-nav-imprimir'
  | 'icon-icon-nav-informacoes'
  | 'icon-icon-nav-login'
  | 'icon-icon-nav-ocultar'
  | 'icon-icon-nav-pasta'
  | 'icon-icon-nav-produtos'
  | 'icon-icon-nav-remover'
  | 'icon-icon-nav-reportar'
  | 'icon-icon-nav-sair'
  | 'icon-icon-nav-seguir'
  | 'icon-icon-nav-selecionar'
  | 'icon-icon-nav-som'
  | 'icon-icon-nav-visualizar'
  | 'icon-icon-nav-voltar'
  | 'icon-icon-info-anunciar'
  | 'icon-icon-info-bloqueio-aberto'
  | 'icon-icon-info-desconto-loja'
  | 'icon-icon-info-dia-noite'
  | 'icon-icon-info-diversidade'
  | 'icon-icon-info-educacao'
  | 'icon-icon-info-emergencia'
  | 'icon-icon-info-empresa'
  | 'icon-icon-info-endereco'
  | 'icon-icon-info-estrategia'
  | 'icon-icon-info-familiar'
  | 'icon-icon-info-foto'
  | 'icon-icon-info-funcionarios'
  | 'icon-icon-info-ideia'
  | 'icon-icon-info-incendio'
  | 'icon-icon-info-informar'
  | 'icon-icon-info-informatica'
  | 'icon-icon-info-invalidez'
  | 'icon-icon-info-ladrao'
  | 'icon-icon-info-lavadeira'
  | 'icon-icon-info-limpeza'
  | 'icon-icon-info-livre-escolha'
  | 'icon-icon-info-localizar-mapa'
  | 'icon-icon-info-loja'
  | 'icon-icon-info-mobile'
  | 'icon-icon-info-morte'
  | 'icon-icon-info-nautico'
  | 'icon-icon-info-outros-seguros'
  | 'icon-icon-info-perfil'
  | 'icon-icon-info-pet'
  | 'icon-icon-info-play'
  | 'icon-icon-info-previdencia'
  | 'icon-icon-info-qualidade'
  | 'icon-icon-info-responsabilidade-civil'
  | 'icon-icon-info-resultado-sorteio'
  | 'icon-icon-info-solucoes-negocio'
  | 'icon-icon-info-traducao'
  | 'icon-icon-info-trofeu'
  | 'icon-icon-info-web'
  | 'icon-icon-fin-acordo'
  | 'icon-icon-fin-banco'
  | 'icon-icon-fin-boleto'
  | 'icon-icon-fin-caixa'
  | 'icon-icon-fin-cartao'
  | 'icon-icon-fin-cheque'
  | 'icon-icon-fin-compra-online'
  | 'icon-icon-fin-comprar'
  | 'icon-icon-fin-compras'
  | 'icon-icon-fin-contribuicao'
  | 'icon-icon-fin-custo'
  | 'icon-icon-fin-debito-online'
  | 'icon-icon-fin-deposito-cheque'
  | 'icon-icon-fin-fatura'
  | 'icon-icon-fin-industria'
  | 'icon-icon-fin-investimento'
  | 'icon-icon-fin-ir'
  | 'icon-icon-fin-nota-fiscal'
  | 'icon-icon-fin-pgbl'
  | 'icon-icon-fin-poupanca'
  | 'icon-icon-fin-preco-acessivel'
  | 'icon-icon-fin-premios'
  | 'icon-icon-fin-receber'
  | 'icon-icon-fin-recibo'
  | 'icon-icon-fin-rendimento'
  | 'icon-icon-fin-resgate'
  | 'icon-icon-fin-sacar-cheque'
  | 'icon-icon-fin-saldo'
  | 'icon-icon-fin-sem-custo'
  | 'icon-icon-fin-simulacao'
  | 'icon-icon-fin-tranquilidade'
  | 'icon-icon-fin-transferencia'
  | 'icon-icon-doc-add-calendario'
  | 'icon-icon-doc-alerta'
  | 'icon-icon-doc-analisar'
  | 'icon-icon-doc-calendario'
  | 'icon-icon-doc-carencia'
  | 'icon-icon-doc-contratos'
  | 'icon-icon-doc-dicionario'
  | 'icon-icon-doc-documento'
  | 'icon-icon-doc-form'
  | 'icon-icon-doc-pdf'
  | 'icon-icon-doc-prancheta'
  | 'icon-icon-doc-protocolos'
  | 'icon-icon-dados-cpf'
  | 'icon-icon-dados-profissao'
  | 'icon-icon-dados-sexo'
  | 'icon-icon-dados-usuario'
  | 'icon-icon-shop-estrela'
  | 'icon-icon-shop-shopping'
  | 'icon-icon-auto-autoridades'
  | 'icon-icon-auto-cambio'
  | 'icon-icon-auto-caminhao'
  | 'icon-icon-auto-coberturas-adicionais'
  | 'icon-icon-auto-completo'
  | 'icon-icon-auto-condutor'
  | 'icon-icon-auto-corretores'
  | 'icon-icon-auto-oficinas-guinchos'
  | 'icon-icon-auto-portas'
  | 'icon-icon-auto-protecao'
  | 'icon-icon-auto-sinistro'
  | 'icon-icon-auto-tracar-rota'
  | 'icon-icon-auto-veiculo'
  | 'icon-icon-atend-adicionar-contato'
  | 'icon-icon-atend-atendente'
  | 'icon-icon-atend-cel'
  | 'icon-icon-atend-celular'
  | 'icon-icon-atend-chat'
  | 'icon-icon-atend-chatbot'
  | 'icon-icon-atend-email-envio'
  | 'icon-icon-atend-email'
  | 'icon-icon-atend-fixo'
  | 'icon-icon-atend-presencial'
  | 'icon-icon-atend-sms'
  | 'icon-icon-atend-telefone'
  | 'icon-icon-alerta-atencao'
  | 'icon-icon-alerta-duvida'
  | 'icon-icon-alerta-erro'
  | 'icon-icon-alerta-sucesso'
  | 'icon-icon-acess-cadeirante'
  | 'icon-icon-acess-font-maior'
  | 'icon-icon-acess-font-menor'
  | 'icon-icon-acess-surdos'
  | 'icon-icon-acess-baixa-visao'
  | 'icon-icon-alerta-erro2'
  | 'icon-icon-atend-bia'
  | 'icon-icon-saude-calendario'
  | 'icon-icon-nav-proibido'
  | 'icon-icon-info-oferta'
  | 'icon-icon-atend-call-center'
  | 'icon-icon-atend-campainha'
  | 'icon-icon-atend-chat-online'
  | 'icon-icon-atend-mensagem-celular'
  | 'icon-icon-auto-app-seguro'
  | 'icon-icon-info-qualificao'
  | 'icon-icon-auto-assistencia'
  | 'icon-icon-auto-onibus'
  | 'icon-icon-info-re'
  | 'icon-icon-fin-receber-valor'
  | 'icon-icon-auto-passageiro'
  | 'icon-icon-auto-posto'
  | 'icon-icon-dados-estado-civil'
  | 'icon-icon-viagem-regresso'
  | 'icon-icon-saude-relatorio-medico'
  | 'icon-icon-dados-grafico'
  | 'icon-icon-dados-meu-cartao'
  | 'icon-icon-doc-renovacao-apolice'
  | 'icon-icon-info-reparos'
  | 'icon-icon-doc-calendario-proximo'
  | 'icon-icon-doc-coleta-documentos'
  | 'icon-icon-saude-viagem'
  | 'icon-icon-doc-consulta'
  | 'icon-icon-doc-documento-facil'
  | 'icon-icon-doc-papel'
  | 'icon-icon-fin-acompanhamento-proposta'
  | 'icon-icon-fin-adicionar-carteira'
  | 'icon-icon-fin-resumo'
  | 'icon-icon-auto-roubo-veiculo'
  | 'icon-icon-fin-cartoes'
  | 'icon-icon-fin-compra-online2'
  | 'icon-icon-info-sala-vip'
  | 'icon-icon-fin-custo-total'
  | 'icon-icon-fin-debito-online2'
  | 'icon-icon-fin-despesas'
  | 'icon-icon-fin-dinheiro'
  | 'icon-icon-doc-2a-via'
  | 'icon-icon-fin-guardar-dinheiro'
  | 'icon-icon-auto-personalizado'
  | 'icon-icon-fin-imposto-renda'
  | 'icon-icon-fin-reembolso'
  | 'icon-icon-info-bilhete-sorteio'
  | 'icon-icon-info-vida'
  | 'icon-icon-info-caixao'
  | 'icon-icon-info-senha-eletronica'
  | 'icon-icon-saude-concierge'
  | 'icon-icon-info-consultoria-sustentavel'
  | 'icon-icon-info-descubra'
  | 'icon-icon-info-embaralhar'
  | 'icon-icon-info-explosao'
  | 'icon-icon-info-familia'
  | 'icon-icon-atend-email-celular'
  | 'icon-icon-info-clique'
  | 'icon-icon-info-galeria-fotos'
  | 'icon-icon-info-importante'
  | 'icon-icon-info-inclusao-exclusao'
  | 'icon-icon-info-infantil'
  | 'icon-icon-info-localizar-sucursal'
  | 'icon-icon-info-pensar'
  | 'icon-icon-info-perfil-corretor'
  | 'icon-icon-info-ticket'
  | 'icon-icon-info-perfil-corretor2'
  | 'icon-icon-info-perfil-corretora'
  | 'icon-icon-info-perfil-dentista'
  | 'icon-icon-info-perfil-medico'
  | 'icon-icon-info-relogio'
  | 'icon-icon-viagem-translado-corpo'
  | 'icon-icon-viagem-translado-medico'
  | 'icon-icon-alerta-transmissao-urgente'
  | 'icon-icon-nav-enviar'
  | 'icon-icon-saude-vacinas'
  | 'icon-icon-alerta-mensagem-urgente'
  | 'icon-icon-nav-mensagem'
  | 'icon-icon-nav-menu-hamburger'
  | 'icon-icon-saude-acidentes'
  | 'icon-icon-viagem-seguro2'
  | 'icon-icon-saude-a-pe'
  | 'icon-icon-saude-cirurgia-plastica'
  | 'icon-icon-saude-curativos'
  | 'icon-icon-saude-despesas-medicas'
  | 'icon-icon-nav-zoom-in'
  | 'icon-icon-nav-zoom-out'
  | 'icon-icon-saude-diaria-hospitalar2'
  | 'icon-icon-saude-dieta'
  | 'icon-icon-saude-farmacias-proximas'
  | 'icon-icon-saude-alimentacao-saudavel'
  | 'icon-icon-saude-funeral'
  | 'icon-icon-saude-ganho-peso'
  | 'icon-icon-saude-hospitais-proximos'
  | 'icon-icon-saude-medicamento'
  | 'icon-icon-saude-perda-peso'
  | 'icon-icon-saude-pilula'
  | 'icon-icon-saude-plano-dental'
  | 'icon-icon-saude-soro'
  | 'icon-icon-seta-longa-baixo-c'
  | 'icon-icon-seta-longa-cima-c'
  | 'icon-icon-vb-beneficios2'
  | 'icon-icon-vb-circuito-cultural'
  | 'icon-icon-vb-exposicoes2'
  | 'icon-icon-seta-dupla-b'
  | 'icon-icon-seta-dupla-direita-b'
  | 'icon-icon-vb-facilidade'
  | 'icon-icon-viagem-acessoria'
  | 'icon-icon-viagem-cancelamento'
  | 'icon-icon-viagem-mala'
  | 'icon-icon-viagem-perda-bagagem'
  | 'icon-icon-info-mover'
  | 'icon-icon-info-unlink'
  | 'icon-icon-info-wifi'
  | 'icon-icon-seta-ponta-baixo'
  | 'icon-icon-seta-ponta-cima'
  | 'icon-icon-seta-ponta-direita'
  | 'icon-icon-seta-ponta-esquerda'
  | 'icon-icon-seta-solida-baixo'
  | 'icon-icon-seta-solida-cima'
  | 'icon-icon-seta-solida-direita'
  | 'icon-icon-seta-solida-esquerda'
  | 'icon-icon-nav-servicos'
  | 'icon-icon-nav-notificacao'
  | 'icon-icon-nav-alvo'
  | 'icon-icon-info-sobre'
  | 'icon-icon-nav-vazio'
  | 'icon-icon-info-circuito-cultural'
  | 'icon-icon-info-noticias'
  | 'icon-icon-nav-exportar'
  | 'icon-icon-nav-filtro'
  | 'icon-icon-nav-check'
  | 'icon-icon-info-status-senha'
  | 'icon-icon-fin-previa-reembolso'
  | 'icon-icon-fin-solicitar-previa-reembolso'
  | 'icon-icon-saude-dental-dentedeleite'
  | 'icon-icon-saude-dental-ideal'
  | 'icon-icon-saude-dental-junior'
  | 'icon-icon-nav-organizar'
  | 'icon-icon-redes-getlink'
  | 'icon-icon-residencial-condominio'
  | 'icon-icon-seta-cima-b'
  | 'icon-icon-vb-servicos'
  | 'icon-icon-viagem-seguro'
  | 'icon-icon-alerta-qrcode-erro'
  | 'icon-icon-alerta-qrcode-sucesso'
  | 'icon-icon-auto-autoline'
  | 'icon-icon-auto-avaliacao-risco'
  | 'icon-icon-auto-restricao'
  | 'icon-icon-doc-clausulas'
  | 'icon-icon-doc-demonstrativo'
  | 'icon-icon-doc-doc'
  | 'icon-icon-doc-html'
  | 'icon-icon-doc-inclusa-itens'
  | 'icon-icon-doc-questionario'
  | 'icon-icon-doc-questionario-avaliacao-risco'
  | 'icon-icon-doc-susep'
  | 'icon-icon-doc-xls'
  | 'icon-icon-fin-formas-pagamento'
  | 'icon-icon-fin-novo-cartao-pronto'
  | 'icon-icon-info-dados-gerais'
  | 'icon-icon-info-dados-renovacao'
  | 'icon-icon-info-local-inspecao'
  | 'icon-icon-info-nome'
  | 'icon-icon-info-objeto-risco'
  | 'icon-icon-info-operacao-especial'
  | 'icon-icon-info-premio-individual'
  | 'icon-icon-info-premio-item'
  | 'icon-icon-info-premio-total'
  | 'icon-icon-info-resumo-itens'
  | 'icon-icon-info-situacao-risco'
  | 'icon-icon-nav-desfazer'
  | 'icon-icon-nav-expandir'
  | 'icon-icon-nav-favoritos'
  | 'icon-icon-nav-login2'
  | 'icon-icon-diagnostico-de-cancer'
  | 'icon-icon-diaria-por-incapacidade'
  | 'icon-icon-dispensa-de-premio'
  | 'icon-icon-dmnho'
  | 'icon-icon-doencas-graves'
  | 'icon-icon-funeral-ampliada'
  | 'icon-icon-lar-kids'
  | 'icon-icon-medica-internacional'
  | 'icon-icon-meu-seguro-bradesco'
  | 'icon-icon-morte-acidental-conjunge'
  | 'icon-icon-motorista-amigo'
  | 'icon-icon-multi-plano'
  | 'icon-icon-sobrevivencia'
  | 'icon-icon-adiantar10'
  | 'icon-icon-alerta'
  | 'icon-icon-assistencia-funeral'
  | 'icon-icon-codigo-barras'
  | 'icon-icon-pause'
  | 'icon-icon-redes-tiktok'
  | 'icon-icon-retroceder10'
  | 'icon-icon-som-mudo'
  | 'icon-icon-som-volume-alto'
  | 'icon-icon-som-volume-baixo'
  | 'icon-icon-telemedicina'
  | 'icon-icon-trem'
  | 'icon-icon-vagas'
  | 'icon-icon-nav-salvar'
  | 'icon-icon-info-endereco-correspondencia'
  | 'icon-icon-info-endereco-de-risco'
  | 'icon-icon-info-restricao-coberturas';
