import React from 'react';
import * as S from './styles';

export interface IArqTextFieldMultilineProps {
  label?: string;
  rows?: number;
  placeholder?: string;
}

export const ArqTextFieldMultiline = ({
  label,
  rows = 5,
  placeholder,
}: IArqTextFieldMultilineProps) => {
  return (
    <>
      {!!label && <S.StyledText>{label}</S.StyledText>}
      <S.StyledMultiline rows={rows} placeholder={placeholder} />
    </>
  );
};
