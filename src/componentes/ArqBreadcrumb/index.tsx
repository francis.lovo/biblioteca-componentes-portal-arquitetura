import React from 'react';
import * as S from './styles';

export interface IArqBreadcrumbProps {
  items?: string[];
  links?: string[];
}
export const ArqBreadcrumb = ({ items, links }: IArqBreadcrumbProps) => {
  return (
    <S.BreadcrumbContainer>
      <S.BreadcrumbItem>
        <a
          href={links && links.length > 0 && links[0] ? links[0] : undefined}
          className="link"
        >
          <i className="icon-icon-vert-residencial" />
        </a>
        {items &&
          items.length > 0 &&
          items.map((item: string, index: number) => {
            return (
              <div
                key={index}
                className={index === items.length - 1 ? 'last' : ''}
              >
                <i className="icon-icon-seta-direita-a" />
                <a
                  href={
                    links && links.length > 0 && links[index + 1]
                      ? links[index + 1]
                      : undefined
                  }
                  className="link"
                >
                  {item}
                </a>
              </div>
            );
          })}
      </S.BreadcrumbItem>
    </S.BreadcrumbContainer>
  );
};
