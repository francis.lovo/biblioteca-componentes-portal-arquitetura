import React from 'react';
import * as S from './styles';

export interface IArqTextFieldProps {
  label?: string;
  onChange?: () => void;
  placeholder?: string;
  value?: string;
}

export const ArqTextField = ({
  label,
  onChange,
  placeholder,
  value,
}: IArqTextFieldProps) => {
  return (
    <>
      <S.StyledTextFieldContainer>{label}</S.StyledTextFieldContainer>
      <S.StyledTextField
        type="text"
        onChange={onChange}
        placeholder={placeholder}
        value={value}
      />
    </>
  );
};
