import React from 'react';
import { LineChart, Line, XAxis, YAxis } from 'recharts';
import * as S from './styles';

interface IDotsChart {
  x: number;
  y: number;
}
export interface IArqLineChartProps {
  dots?: IDotsChart[];
  chartWidth?: number;
  chartHeight?: number;
}

export const ArqLineChart = ({
  dots = [
    { x: 1, y: 1 },
    { x: 2, y: 2 },
  ],
  chartWidth = 800,
  chartHeight = 300,
}: IArqLineChartProps) => (
  <S.ChartContainer>
    <LineChart width={chartWidth} height={chartHeight} data={dots}>
      <XAxis dataKey="x" />
      <YAxis dataKey="y" />
      <Line dataKey="y" stroke="#8884d8" dot={{ fill: 'blue', r: 4 }} />
    </LineChart>
  </S.ChartContainer>
);
