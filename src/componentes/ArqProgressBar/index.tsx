import React from 'react';
import * as S from './styles';

export interface IArqProgressBarProps {
  porcentGreen?: number;
  porcentBlue?: number;
  porcentYellow?: number;
  porcentRed?: number;
}

export const ArqProgressBar = ({
  porcentBlue = 100,
  porcentGreen,
  porcentYellow,
  porcentRed,
}: IArqProgressBarProps) => {
  return (
    <S.ProgressBarWrapper>
      <S.ProgressBar>
        <S.Text>0%</S.Text>
        <S.BlueProgress porcentBlue={porcentBlue} />
        {!!porcentBlue && (
          <S.TextPorcent porcent={porcentBlue}>{porcentBlue}%</S.TextPorcent>
        )}
        {!!porcentGreen && (
          <S.GreenProgress
            porcentGreen={porcentGreen}
            porcentBlue={porcentBlue}
          />
        )}
        {!!porcentGreen && (
          <S.TextPorcent porcent={porcentBlue + porcentGreen}>
            {porcentBlue + porcentGreen}%
          </S.TextPorcent>
        )}
        {!!porcentYellow && !!porcentGreen && (
          <S.YellowProgress
            porcentYellow={porcentYellow}
            lastPorcent={porcentGreen + porcentBlue}
          />
        )}
        {!!porcentGreen && !!porcentYellow && (
          <S.TextPorcent porcent={porcentBlue + porcentGreen + porcentYellow}>
            {porcentBlue + porcentGreen + porcentYellow}%
          </S.TextPorcent>
        )}
        {!!porcentRed && !!porcentYellow && !!porcentGreen && (
          <S.RedProgress
            porcentRed={porcentRed}
            lastPorcent={porcentYellow + porcentBlue + porcentGreen}
          />
        )}
        <S.Text5>100%</S.Text5>
      </S.ProgressBar>
    </S.ProgressBarWrapper>
  );
};
