import React from 'react';
import * as S from './styles';

export interface IArqGlossaryProps {
  items?: string[];
  onChange?: (items: string[]) => {};
}
export const ArqGlossary = ({ items, onChange }: IArqGlossaryProps) => {
  const letters = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
  ];
  const [selected, setSelected] = React.useState('a');
  const [realItems, setRealItems] = React.useState(items);
  const changeSelected = (value: string) => {
    setSelected(value);
  };
  const addNewterm = () => {
    if (onChange && realItems) {
      onChange(realItems);
    } else if (onChange && !realItems) {
      onChange([]);
    }
  };

  const initializeScrollbar = () => {
    // Create custom scrollbar functionality
    const container = document.getElementById('scrollableContainer');
    if (container) {
      const scrollbar = document.createElement('div');
      scrollbar.className = 'custom-scrollbar';

      const thumb = document.createElement('div');
      thumb.className = 'custom-scrollbar-thumb';
      scrollbar.appendChild(thumb);

      container.appendChild(scrollbar);
    }
  };
  React.useEffect(() => {
    // Initialize custom scrollbar when component mounts
    const container = document.getElementById('scrollableContainer');
    if (container) {
      container.addEventListener('mouseenter', initializeScrollbar);
    }

    return () => {
      // Clean up event listener when component unmounts
      if (container) {
        container.removeEventListener('mouseenter', initializeScrollbar);
      }
    };
  }, []);
  React.useEffect(() => {
    setRealItems(items);
  }, [items]);
  return (
    <S.StyledGlossaryContainer>
      <S.StyledLettersContainer>
        {letters.map((letter: string) => {
          return (
            <S.StyledLetterButton
              onClick={() => changeSelected(letter)}
              selected={selected === letter}
              href={`#${letter}`}
            >
              {letter.toLocaleUpperCase()}
            </S.StyledLetterButton>
          );
        })}
      </S.StyledLettersContainer>
      <S.StyledDropDownGlossaryContainer>
        <S.ScrollableContainer id="scrollableContainer">
          {letters.map((letter: string, index: number) => {
            return (
              <>
                <S.StyledDropDownLetter first={index === 0} id={letter}>
                  {letter.toLocaleUpperCase()}
                </S.StyledDropDownLetter>
                {realItems &&
                  realItems.length > 0 &&
                  realItems
                    .sort(Intl.Collator().compare)
                    .map((item: string) => {
                      if (
                        item.charAt(0) === letter ||
                        item.charAt(0) === letter.toLocaleUpperCase()
                      ) {
                        return <S.StyledItem>{item}</S.StyledItem>;
                      } else {
                        return '';
                      }
                    })}
              </>
            );
          })}
        </S.ScrollableContainer>
      </S.StyledDropDownGlossaryContainer>
      <S.StyledNewTermContainer onClick={addNewterm}>
        <S.StyledNewTermIcon className="icon-icon-nav-adicionar" />
        <S.StyledNewTermText>Novo termo</S.StyledNewTermText>
      </S.StyledNewTermContainer>
    </S.StyledGlossaryContainer>
  );
};
