import React from 'react';
import styled from 'styled-components';

export interface IArqCardBirthdayProps {
  name?: string;
  date?: string;
  info?: string;
}

// Estilos do componente
const CardWrapper = styled.div`
  border: none;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 250px;
  height: 70px;
  font-family: Bradesco Regular Sans;
  margin-bottom: 0px;
`;

const NameAndDateWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 4px;
`;

const Icon = styled.img`
  width: 18px;
  height: 18px;
  margin-bottom: 6px;
  margin-right: 8px;
`;

const NameText = styled.p`
  color: gray;
  font-weight: bold;
  margin: 0;
`;

const DateText = styled.p`
  color: lightgray;
  margin: 0px 10px;
`;

const InfoText = styled.p`
  color: lightgray;
  margin: 0;
`;

// Componente ArqCardBirthday
export const ArqCardBirthday = ({
  info,
  name = 'Name',
  date = '12/01',
}: IArqCardBirthdayProps) => {
  return (
    <CardWrapper>
      <NameAndDateWrapper>
        <Icon src={require('../../assets/birthday.png')} alt="Birthday Icon" />
        <NameText>{name}</NameText>
        <DateText>{date}</DateText>
      </NameAndDateWrapper>
      {!!info && <InfoText>{info}</InfoText>}
    </CardWrapper>
  );
};
