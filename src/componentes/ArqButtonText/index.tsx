import React from 'react';
import * as S from './styles';

export interface IArqButtonTextProps {
  previous?: boolean;
  next?: boolean;
  onClickPrevious?: () => void;
  onClickNext?: () => void;
}

export const ArqButtonText = ({
  previous,
  next,
  onClickPrevious,
  onClickNext,
}: IArqButtonTextProps) => {
  return (
    <S.StyledContainer>
      {!!previous && (
        <S.ButtonContainer next={next} onClick={onClickPrevious}>
          <i className="icon-icon-seta-esquerda-b" />
          <S.StyledText type="previous">Anterior</S.StyledText>
        </S.ButtonContainer>
      )}
      {!!next && (
        <S.ButtonContainer onClick={onClickNext}>
          <S.StyledText type="next">Proximo</S.StyledText>
          <i className="icon-icon-seta-direita-b" />
        </S.ButtonContainer>
      )}
    </S.StyledContainer>
  );
};
