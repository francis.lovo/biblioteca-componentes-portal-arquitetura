import styled from 'styled-components';

export const ButtonContainer = styled.div<{
  next?: boolean;
}>`
  color: #cc092f;
  background-color: transparent;
  font-family: 'Bradesco Regular Sans';
  font-weight: bold;
  font-size: 14px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  margin-right: ${({ next }) => (next ? '15px' : '')};
`;
export const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const StyledText = styled.div<{ type: 'next' | 'previous' }>`
  margin-left: ${({ type }) => (type === 'previous' ? '3px' : '')};
  margin-right: ${({ type }) => (type === 'next' ? '3px' : '')};
`;
