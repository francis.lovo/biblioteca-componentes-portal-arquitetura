import styled from 'styled-components';

export const StyledImageButton = styled.div`
  display: flex;
  flex-direction: column;
  width: max-content;
  box-shadow: 0 0 6px rgba(0, 0, 0, 0.1), 4px 0 6px rgba(0, 0, 0, 0.1);
  cursor: pointer;
  width: 100%;
`;
export const StyledImage = styled.img`
  zindex: 1001;
  width: 100%;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 0px;
  border-bottom-left-radius: 0px;
`;
