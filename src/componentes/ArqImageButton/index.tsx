import React from 'react';
import * as S from './styles';
import { ArqChapterButton } from '../ArqChapterButton';

export interface IArqImageButtonProps {
  title?: string;
  subtitle?: string;
  link?: string;
  src?: string;
}

export const ArqImageButton = ({
  title = 'title',
  subtitle,
  link,
  src,
}: IArqImageButtonProps) => {
  return (
    <S.StyledImageButton>
      {!!src && <S.StyledImage src={src} alt="ImageButton" />}
      <ArqChapterButton
        titles={[title]}
        subtitles={subtitle ? [subtitle] : undefined}
        color="rgb(251, 249, 249)"
        links={link ? [link] : undefined}
      />
    </S.StyledImageButton>
  );
};
