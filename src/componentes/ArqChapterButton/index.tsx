import React from 'react';
import * as S from './styles';

export interface IArqChapterButtonProps
  extends Partial<S.IArqChapterButtonProps> {
  titles?: string[];
  subtitles?: string[];
  links?: string[];
}

export const ArqChapterButton = ({
  titles = ['title'],
  color = 'rgb(73, 5, 5)',
  subtitles,
  open = false,
  links,
}: IArqChapterButtonProps) => {
  return (
    <div style={{ width: '100%' }}>
      {titles.map((title: string, index: number) => {
        if (open || (!open && index === 0)) {
          return (
            <S.StyledChapterButton
              color={color}
              open={open}
              index={index}
              last={titles.length - 1}
              key={index}
              href={
                !!links && links.length > 0 && !!links[index]
                  ? links[index]
                  : undefined
              }
            >
              <S.StyledChapterButtonTexts>
                <S.StyledChapterButtonTitle index={index}>
                  {title}
                </S.StyledChapterButtonTitle>
                {!!subtitles && subtitles.length > 0 && !!subtitles[index] && (
                  <S.StyledChapterButtonSubtitle>
                    {subtitles[index]}
                  </S.StyledChapterButtonSubtitle>
                )}
              </S.StyledChapterButtonTexts>
              <S.StyledChapterButtonIcon className="icon-icon-seta-direita-b" />
            </S.StyledChapterButton>
          );
        } else {
          return '';
        }
      })}
    </div>
  );
};
