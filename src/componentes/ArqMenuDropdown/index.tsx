import React from 'react';
import * as S from './styles';
interface IItems {
  name: string;
  dropdownItems: string[];
}
export interface IArqMenuDropdownProps {
  items?: IItems[];
  title?: string;
}

export const ArqMenuDropdown = ({
  items,
  title = 'Menu',
}: IArqMenuDropdownProps) => {
  const [showDrop, setShowDrop] = React.useState<number[]>([]);
  const handleClick = (index: number) => {
    const aux: number[] = [];
    let achou = false;
    if (showDrop.length > 0) {
      showDrop.forEach((drop: number) => {
        if (drop !== index) {
          aux.push(drop);
        } else {
          achou = true;
        }
      });
    }
    if (!achou) {
      aux.push(index);
    }
    setShowDrop(aux);
  };
  const buscarDrop = (index: number) => {
    let achouDrop = false;
    if (showDrop.length > 0) {
      showDrop.forEach((drop: number) => {
        if (index === drop) {
          achouDrop = true;
        }
      });
    }
    return achouDrop;
  };
  return (
    <S.StyledContainer>
      <S.StyledTitle>{title}</S.StyledTitle>
      {items &&
        items.length > 0 &&
        items.map((item: IItems, index: number) => {
          return (
            <S.StyledItemsContainer>
              <S.StyledItemName onClick={() => handleClick(index)}>
                <S.StyledItems>{item.name}</S.StyledItems>
                {item.dropdownItems && item.dropdownItems.length > 0 && (
                  <S.StyledItemIcon
                    className={
                      buscarDrop(index)
                        ? 'icon-icon-seta-cima-a'
                        : 'icon-icon-seta-baixo-a'
                    }
                  />
                )}
              </S.StyledItemName>
              {!!item.dropdownItems &&
                item.dropdownItems.length > 0 &&
                item.dropdownItems.map((drop: string) => {
                  return (
                    <S.StyledDropdownItem show={buscarDrop(index)}>
                      {drop}
                    </S.StyledDropdownItem>
                  );
                })}
            </S.StyledItemsContainer>
          );
        })}
    </S.StyledContainer>
  );
};
