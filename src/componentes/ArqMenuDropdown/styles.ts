import styled from 'styled-components';

export const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
  font-family: 'Bradesco Regular Sans';
  color: #4f4f4f;
  padding: 15px;
`;
export const StyledTitle = styled.div`
  width: 100%;
  font-size: 18px;
  font-weight: bold;
`;
export const StyledItems = styled.div`
  font-size: 14px;
  font-weight: bold;
`;
export const StyledItemsContainer = styled.div`
  width: 25%;
  margin-top: 25px;
  display: flex;
  flex-direction: column;
  height: auto;
`;
export const StyledItemName = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  cursor: pointer;
`;
export const StyledItemIcon = styled.i`
  font-weight: 900;
  font-size: 14px;
  margin-left: 5px;
`;
export const StyledDropdownItem = styled.div<{ show: boolean }>`
  font-size: 14px;
  margin-left: 10px;
  margin-top: 10px;
  color: ${({ show }) => (show ? '#4f4f4f' : 'transparent')};
`;
