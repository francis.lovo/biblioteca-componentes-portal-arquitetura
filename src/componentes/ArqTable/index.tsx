import React from 'react';
import * as S from './styles';

export interface IArqTableProps {
  topLabels?: string[];
  tableItems?: string[][];
}

export const ArqTable = ({
  tableItems = [['item1']],
  topLabels = ['table'],
}: IArqTableProps) => {
  return (
    <S.TableWrapper>
      <thead>
        <tr>
          {!!topLabels &&
            topLabels.length > 0 &&
            topLabels.map(label => {
              return <S.TableHeader>{label}</S.TableHeader>;
            })}
        </tr>
      </thead>
      <tbody>
        {!!tableItems &&
          tableItems.length > 0 &&
          tableItems.map((tableItem, index) => (
            <S.TableRow key={index} even={index % 2 === 0}>
              {tableItem.map(item => {
                return <S.TableCell>{item}</S.TableCell>;
              })}
            </S.TableRow>
          ))}
      </tbody>
    </S.TableWrapper>
  );
};
