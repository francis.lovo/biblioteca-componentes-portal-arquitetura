import React from 'react';
import * as S from './styles';

export interface IArqImageSliderProps {
  current?: number;
  images: string[];
}

export const ArqImageSlider = ({
  current = 0,
  images,
}: IArqImageSliderProps) => {
  const [currentIndex, setCurrentIndex] = React.useState(current);

  const goToNextSlide = () => {
    setCurrentIndex(prevIndex =>
      prevIndex === images.length - 1 ? 0 : prevIndex + 1
    );
  };

  const goToPrevSlide = () => {
    setCurrentIndex(prevIndex =>
      prevIndex === 0 ? images.length - 1 : prevIndex - 1
    );
  };

  return (
    <S.StyledSlider>
      <S.StyledButton onClick={goToPrevSlide} previous={true}>
        <S.StyledrButtonIcon
          src={require('../../icons/seta.svg')}
          alt="Icon"
          previous={true}
        />
      </S.StyledButton>
      <S.StyledButton onClick={goToNextSlide}>
        <S.StyledrButtonIcon src={require('../../icons/seta.svg')} alt="Icon" />
      </S.StyledButton>

      <S.StyledSlideContainer>
        {images.map((image, index) => (
          <S.StyledImage
            key={index}
            src={image}
            alt={`Slide ${index + 1}`}
            current={currentIndex}
            index={index}
          />
        ))}
      </S.StyledSlideContainer>
      <S.StyledSliderIndicators>
        {images.map((_image, index) => (
          <S.StyledSliderIndicator
            key={index}
            onClick={() => setCurrentIndex(index)}
            current={currentIndex}
            index={index}
          />
        ))}
      </S.StyledSliderIndicators>
    </S.StyledSlider>
  );
};
