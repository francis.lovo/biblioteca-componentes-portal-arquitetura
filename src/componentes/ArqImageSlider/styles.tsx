import styled, { css } from 'styled-components';

export interface ICurrentImageProps {
  current?: number;
  index?: number;
}
export interface IButtonPrps {
  previous?: boolean;
}
export const StyledSlider = styled.div`
  position: relative;
  width: 600px;
  height: 400px;
  overflow: hidden;
  font-family: Bradesco Regular Sans;
`;
export const StyledButton = styled.button<IButtonPrps>`
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 200px;
  transform: translateY(-50%);
  background-color: #fff;
  border: none;
  outline: none;
  cursor: pointer;
  border-radius: 20px;
  ${({ previous }) =>
    previous
      ? css`
          left: 20px;
        `
      : css`
          right: 20px;
        `};
  width: 20px;
  height: 20px;
`;
export const StyledrButtonIcon = styled.img<IButtonPrps>`
  height: 13px;
  ${({ previous }) =>
    previous
      ? css`
          transform: rotate(180deg);
        `
      : ''}
`;
export const StyledImage = styled.img<ICurrentImageProps>`
  width: 600px;
  object-fit: cover;
  opacity: 0;
  transition: opacity 0.5s ease-in-out;
  ${({ current, index }) =>
    current === index
      ? css`
          opacity: 1;
        `
      : ''}
  position: absolute;
  z-index: -10;
`;

export const StyledSlideContainer = styled.div`
  display: flex;
  transition: transform 0.5s ease-in-out;
  width: 600px;
`;

export const StyledSliderIndicators = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 10px;
  position: absolute;
  transform: translateY(-50%);
  top: 80%;
  width: 100%;
`;

export const StyledSliderIndicator = styled.span<ICurrentImageProps>`
  display: block;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: rgba(0, 0, 0, 0.5);
  margin: 0 5px;
  cursor: pointer;
  ${({ current, index }) =>
    current === index
      ? css`
          background-color: #fff;
        `
      : ''}
`;
