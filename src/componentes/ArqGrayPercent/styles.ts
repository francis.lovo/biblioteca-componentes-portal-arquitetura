import styled from 'styled-components';

export const GrayPercentWrapper = styled.div`
  display: flex;
  align-items: center;
  font-family: 'Bradesco Regular Sans';
  width: 100%;
`;

export const GrayProgressBar = styled.div<{ percent?: number }>`
  height: 15px;
  width: ${({ percent }) => percent + '%'};
  background-color: gray;
  border-radius: 5px;
`;

export const GrayText = styled.div`
  margin-left: 5px;
  font-size: 12px;
`;
