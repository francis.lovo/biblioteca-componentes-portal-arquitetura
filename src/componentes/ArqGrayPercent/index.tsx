import React from 'react';
import * as S from './styles';

export interface IArqGrayPercent {
  percent?: number;
}
export const ArqGrayPercent = ({ percent = 100 }: IArqGrayPercent) => {
  return (
    <S.GrayPercentWrapper>
      <S.GrayProgressBar percent={percent} />
      <S.GrayText>{percent}%</S.GrayText>
    </S.GrayPercentWrapper>
  );
};
