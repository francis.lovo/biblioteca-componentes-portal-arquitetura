import React, { useState } from 'react';
import styled from 'styled-components';
import { RedBradesco } from '../../colors';

export interface IArqStepperProps {
  numberItems?: number;
}

const StyledArqStepper = styled.div<IArqStepperProps>`
  display: flex;
  align-items: center;
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  span {
    display: inline-flex;
    justify-content: center;
    align-items: center;
    width: 24px;
    height: 24px;
    border-radius: 50%;
    border: 1px solid ${RedBradesco};
    cursor: pointer;
    color: ${RedBradesco};
    background-color: white;
  }

  span.selected {
    color: white;
    background-color: ${RedBradesco};
  }
  flex-direction: row;
`;
const StyledLine = styled.div`
  border-top: 1px solid ${RedBradesco};
  width: 15px;
`;

export const ArqStepper = ({ numberItems = 1 }: IArqStepperProps) => {
  const [selectedStep, setSelectedStep] = useState<number | null>(null);

  const handleStepClick = (step: number) => {
    setSelectedStep(step);
  };

  return (
    <StyledArqStepper>
      {Array.from({ length: numberItems }, (_, index) => index + 1).map(
        (step, index) => (
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <span
              key={step}
              onClick={() => handleStepClick(step)}
              className={step === selectedStep ? 'selected' : ''}
            >
              {step}
            </span>
            {index < numberItems - 1 && <StyledLine />}
          </div>
        )
      )}
    </StyledArqStepper>
  );
};
