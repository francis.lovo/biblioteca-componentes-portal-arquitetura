import React from 'react';
import * as S from './styles';

export interface IArqPhotoInfoCardProps {
  photo?: string;
  title?: string;
  name?: string;
  email?: string;
}
export const ArqPhotoInfoCard = ({
  title = 'title',
  name,
  email,
  photo,
}: IArqPhotoInfoCardProps) => {
  return (
    <S.StyledContainer>
      {!photo && <S.StyledNoPhoto />}
      {!!photo && (
        <S.StyledPhoto>
          <S.StyledImg src={photo} alt="InfoPhoto" />
        </S.StyledPhoto>
      )}
      <S.StyledTextsContainer>
        <S.StyledTitle>{title}</S.StyledTitle>
        {!!name && <S.StyledName>{name}</S.StyledName>}
        {!!email && <S.StyledEmail>{email}</S.StyledEmail>}
      </S.StyledTextsContainer>
    </S.StyledContainer>
  );
};
