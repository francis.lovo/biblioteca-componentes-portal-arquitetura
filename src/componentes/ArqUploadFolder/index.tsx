import React from 'react';
import { ArqIconButton } from '../ArqIconButton';
import * as S from './styles';

export interface IArqUploadFolderProps {
  label?: string;
  files: any;
  setFiles: React.Dispatch<any>;
  color?: string;
  hoverColor?: string;
  plusIcon?: boolean;
}

export const ArqUploadFolder = ({
  label,
  files,
  setFiles,
  color = '#D5D5D5',
  hoverColor = '#B3B3B3',
  plusIcon,
}: IArqUploadFolderProps) => {
  const buttonUpload = React.useRef<any>();
  const handleClick = () => {
    if (buttonUpload) {
      buttonUpload.current.click();
    }
  };
  const handleChange = (event: any) => {
    const quantidadeArquivos = event.target.files.length;
    const arrayAux: any[] = [];

    for (let index = 0; index < quantidadeArquivos; index++) {
      arrayAux.push(event.target.files[index]);
    }
    setFiles(arrayAux);
  };
  React.useEffect(() => {
    console.log('FILES: ', files);
  }, [files]);
  return (
    <>
      {!!label && <S.StyledText>{label}</S.StyledText>}
      <S.StyledUploaderContainer>
        <S.StyleduploaderButton
          onClick={handleClick}
          color={color}
          hoverColor={hoverColor}
        >
          <input
            type="file"
            style={{ display: 'none' }}
            ref={buttonUpload}
            onChange={handleChange}
          />
          Escolha o Arquivo
        </S.StyleduploaderButton>
        {plusIcon && (
          <ArqIconButton
            icon="icon-icon-nav-adicionar"
            color={color}
            hoverColor={hoverColor}
            onClick={handleClick}
          />
        )}
      </S.StyledUploaderContainer>
      {files && files.length > 0 && (
        <S.StyledFileName>{files[0].name}</S.StyledFileName>
      )}
    </>
  );
};
