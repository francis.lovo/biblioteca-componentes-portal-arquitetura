import React from 'react';
import * as S from './styles';

export interface ISelect {
  value: string | number;
  label: string;
  disabled?: boolean;
}

export interface IArqSelectInputProps {
  options?: ISelect[];
  noSelectedLabel?: string;
  label?: string;
}

export const ArqSelectInput = ({
  options,
  noSelectedLabel,
  label,
}: IArqSelectInputProps) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [selectedOptionLabel, setSelectedOptionLabel] = React.useState<
    string
  >();
  const [selectedOption, setSelectedOption] = React.useState<ISelect>();
  const chooseOp = (option: ISelect) => {
    setSelectedOptionLabel(option.label);
    setSelectedOption(option);
    setIsOpen(false);
  };
  return (
    <div>
      {!!label && <S.StyledLabel>{label}</S.StyledLabel>}
      <S.DropdownContainer>
        <S.DropdownButton
          onClick={() => {
            setIsOpen(!isOpen);
          }}
        >
          <S.DropdownButtonText>
            {selectedOptionLabel
              ? selectedOptionLabel
              : noSelectedLabel
              ? noSelectedLabel
              : 'Selecione'}
          </S.DropdownButtonText>
          <S.ArrowIcon className="icon-icon-seta-baixo-b" />
        </S.DropdownButton>
        <S.DropdownList isOpen={isOpen}>
          {!!options &&
            options?.map((option: ISelect) => {
              return (
                <S.DropdownItem
                  onClick={() => {
                    !option.disabled ? chooseOp(option) : {};
                  }}
                  selected={selectedOption === option}
                  isDisabled={option.disabled ? option.disabled : false}
                >
                  {option.label}
                </S.DropdownItem>
              );
            })}
        </S.DropdownList>
      </S.DropdownContainer>
    </div>
  );
};
