import styled from 'styled-components';

export const StyledDatePicker = styled.div`
  border: 1px solid #b8b8b8;
  padding: 10px;
  width: 100%;
  border-radius: 4px;
  font-family: Bradesco Regular Sans;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
`;
export const StyledHiddenIput = styled.input`
  height: 0;
  border-color: transparent;
  :focus-visible {
    outline: none;
  }
  width: 1px;
`;
export const StyledText = styled.div<{ selected?: boolean }>`
  color: ${({ selected }) => (selected ? '#4f4f4f' : '#b8b8b8')};
`;
export const StyledTextFieldContainer = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #4f4f4f;
  margin-bottom: 10px;
`;
