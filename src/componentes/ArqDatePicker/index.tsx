import React from 'react';
import * as S from './styles';
import moment from 'moment';
import { ArqIconButton } from '../ArqIconButton';

export interface IArqDatePickerProps {
  label?: string;
  value?: string;
  onChange?: (value: string) => void;
  min?: string;
  max?: string;
}

export const ArqDatePicker = ({
  label,
  value,
  onChange,
  min,
  max,
}: IArqDatePickerProps) => {
  const [dateValue, setDateValue] = React.useState<string | undefined>(value);
  const inputDate = React.useRef<any>();
  const handleClick = () => {
    if (inputDate) {
      inputDate.current.showPicker();
    }
  };
  const onChangeDate = () => {
    setDateValue(moment(inputDate.current.value).format('DD/MM/YYYY'));
    if (onChange) {
      onChange(inputDate.current.value);
    }
  };

  React.useEffect(() => {
    setDateValue(value);
  }, [value]);
  return (
    <div>
      {!!label && (
        <S.StyledTextFieldContainer>{label}</S.StyledTextFieldContainer>
      )}
      <S.StyledDatePicker onClick={handleClick}>
        <S.StyledText selected={dateValue !== undefined}>
          {dateValue ? dateValue : '00/00/00'}
        </S.StyledText>
        <div
          style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}
        >
          <ArqIconButton icon="icon-icon-doc-calendario" color="#b8b8b8" />
          <S.StyledHiddenIput
            type="date"
            ref={inputDate}
            onChange={onChangeDate}
            min={min}
            max={max}
          />
        </div>
      </S.StyledDatePicker>
    </div>
  );
};
