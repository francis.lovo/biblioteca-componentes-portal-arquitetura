import React from 'react';
import * as S from './styles';

export interface IArqHorizointalChartProps {
  labels?: string[];
  porcents?: number[];
}

export const ArqHorizontalChart = ({
  labels,
  porcents = [100],
}: IArqHorizointalChartProps) => {
  return (
    <S.Container>
      <S.BarChartContainer>
        <S.BarChart>
          {porcents.map((numb: number, index) => {
            return (
              <S.BarContainer key={index}>
                {labels && labels.length > 0 && labels[index] && (
                  <S.Text>{labels[index]}</S.Text>
                )}
                <S.EmptyBar>
                  <S.FilledBar progress={numb} />
                </S.EmptyBar>
              </S.BarContainer>
            );
          })}
        </S.BarChart>
      </S.BarChartContainer>
    </S.Container>
  );
};
