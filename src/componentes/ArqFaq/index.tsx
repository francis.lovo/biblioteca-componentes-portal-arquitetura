import React from 'react';
import * as S from './styles';

interface IData {
  itemNames: string[];
  itemTexts: string[];
}

export interface IArqFaqProps {
  buttons?: string[];
  data?: IData[];
}

export const ArqFaq = ({ buttons, data }: IArqFaqProps) => {
  const [buttonSelected, setButtonSelected] = React.useState(0);
  const [itemSelected, setItemSelected] = React.useState(-1);
  return (
    <S.StyledFaqContainer>
      <S.StyledButtonCantainer>
        {buttons &&
          buttons.length > 0 &&
          buttons.map((buttonString: string, index: number) => {
            return (
              <S.StyledButton
                buttonSelected={buttonSelected === index}
                onClick={() => {
                  setButtonSelected(index);
                  setItemSelected(-1);
                }}
              >
                {buttonString}
              </S.StyledButton>
            );
          })}
      </S.StyledButtonCantainer>
      <S.StyledItemsContainer>
        {!!data &&
          data.length > 0 &&
          data.map((dataI: IData, index: number) => {
            return (
              <>
                {dataI.itemNames.map((itemName: string, i: number) => {
                  return (
                    <>
                      {index === buttonSelected && (
                        <S.StyledItem>
                          <S.StyledTitleItem>
                            {itemName}
                            {dataI.itemTexts &&
                              dataI.itemTexts.length > 0 &&
                              dataI.itemTexts[i] && (
                                <S.StyledIcon
                                  className={
                                    itemSelected === i
                                      ? 'icon-icon-seta-cima-a'
                                      : 'icon-icon-seta-baixo-a'
                                  }
                                  onClick={() => {
                                    if (itemSelected !== i) {
                                      setItemSelected(i);
                                    } else {
                                      setItemSelected(-1);
                                    }
                                  }}
                                />
                              )}
                          </S.StyledTitleItem>
                          {itemSelected === i &&
                            dataI.itemTexts &&
                            dataI.itemTexts.length > 0 &&
                            dataI.itemTexts[i] && (
                              <S.StyledTextItem>
                                {dataI.itemTexts[i]}
                              </S.StyledTextItem>
                            )}
                        </S.StyledItem>
                      )}
                    </>
                  );
                })}
              </>
            );
          })}
      </S.StyledItemsContainer>
    </S.StyledFaqContainer>
  );
};
