import styled from 'styled-components';
export const StyledIcon = styled.i`
  color: #04ac4c;
  font-size: 100px;
  margin-top: 15px;
  margin-bottom: 15px;
`;
export const WarningMessage = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #4f4f4f;
  font-size: 20px;
  margin-bottom: 15px;
`;
export const StyledModalContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`;
export const MessageContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #ececec;
  padding-top: 20px;
  padding-bottom: 20px;
`;
