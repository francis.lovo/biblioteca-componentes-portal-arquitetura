import React from 'react';
import { ArqButton } from '../ArqButton';
import { ArqModal } from '../ArqModal';
import * as S from './styles';

export interface IArqSuccessModalProps {
  isOpen?: boolean;
  onClose?: () => void;
  onClick?: () => void;
}

export const ArqSuccessModal = ({
  isOpen,
  onClose,
  onClick,
}: IArqSuccessModalProps) => {
  return (
    <ArqModal
      topLine={true}
      modalWidth="300px"
      isOpen={isOpen}
      onClose={onClose}
      noCloseButton={true}
      padding={0}
    >
      <S.StyledModalContainer>
        <S.StyledIcon className="icon-icon-alerta-sucesso" />
        <S.MessageContainer>
          <S.WarningMessage>Enviado com Sucesso</S.WarningMessage>
          <ArqButton text="Ok" onClick={onClick} />
        </S.MessageContainer>
      </S.StyledModalContainer>
    </ArqModal>
  );
};
