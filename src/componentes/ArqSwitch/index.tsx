import React from 'react';
import * as S from './styles';

export interface IArqSwitchProps {
  onChange?: (value: boolean) => void;
  isOn?: boolean;
  label?: string;
}

export const ArqSwitch = ({
  label = 'label',
  isOn = false,
}: IArqSwitchProps) => {
  const [isChecked, setIsChecked] = React.useState(isOn);

  const handleSwitch = () => {
    setIsChecked(!isChecked);
  };

  React.useEffect(() => {
    setIsChecked(isOn);
  }, [isOn]);
  return (
    <>
      <S.SwitchContainer isOn={isChecked}>
        <S.SwitchInput checked={isChecked} onChange={handleSwitch} />
        <div className="switch" />
        <S.StyledSwitchLabel>{label}</S.StyledSwitchLabel>
      </S.SwitchContainer>
    </>
  );
};
