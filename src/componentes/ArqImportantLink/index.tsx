import React from 'react';
import * as S from './styles';

export interface IArqImportantLinkProps {
  href?: string;
  text?: string;
}

export const ArqImportantLink = ({
  href,
  text = 'Important Link',
}: IArqImportantLinkProps) => {
  return (
    <S.StyledImportantLink href={href}>
      <S.StyledLinkContainer>
        {text}
        <S.StyledLinkIcon
          src={require('../../assets/link-externo.png')}
          alt="Icon"
        />
      </S.StyledLinkContainer>
    </S.StyledImportantLink>
  );
};
