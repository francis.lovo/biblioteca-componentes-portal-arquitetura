import styled, { css } from 'styled-components';

export interface IArqNotificationCardProps {
  text?: string;
  footer?: string;
}

export const StyledCardContainer = styled.div`
  border: 1px solid #e8e8e8;
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: 15px;
  padding-top: 15px;
  border-radius: 4px;
  display: flex;
  justify-content: space-between;
  font-family: Bradesco Regular Sans;
`;
export const StyledTitle = styled.div<IArqNotificationCardProps>`
  font-size: 18px;
  font-weight: bold;
  ${({ text, footer }) =>
    (footer && footer !== '') || (text && text !== '')
      ? css`
          margin-bottom: 10px;
        `
      : ''}
`;
export const StyledSubtitle = styled.div<IArqNotificationCardProps>`
  font-size: 14px;
  ${({ footer }) =>
    footer && footer !== ''
      ? css`
          margin-bottom: 10px;
        `
      : ''}
`;
export const StyledFooter = styled.div`
  color: #dcdcdc;
  font-size: 10px;
`;
export const StyledTextContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  flex-direction: column;
`;
export const StyledIconContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: end;
`;
