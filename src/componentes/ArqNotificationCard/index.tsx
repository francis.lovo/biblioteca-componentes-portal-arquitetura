import React from 'react';
import * as S from './styles';
import { ArqIconButton } from '../ArqIconButton';

export interface IArqNotificationCardProps
  extends Partial<S.IArqNotificationCardProps> {
  title?: string;
  onClick?: () => void;
}

export const ArqNotificationCard = ({
  title = 'title',
  text,
  footer,
  onClick,
}: IArqNotificationCardProps) => {
  return (
    <S.StyledCardContainer>
      <S.StyledTextContainer>
        <S.StyledTitle text={text} footer={footer}>
          {title}
        </S.StyledTitle>
        {!!text && <S.StyledSubtitle footer={footer}>{text}</S.StyledSubtitle>}
        {!!footer && <S.StyledFooter>{footer}</S.StyledFooter>}
      </S.StyledTextContainer>
      <S.StyledIconContainer>
        <ArqIconButton
          icon="icon-icon-residencial-lixeira"
          fontSize="18px"
          color="#dcdcdc"
          onClick={onClick}
        />
      </S.StyledIconContainer>
    </S.StyledCardContainer>
  );
};
