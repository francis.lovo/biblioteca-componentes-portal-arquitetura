import styled, { css } from 'styled-components';

export const RadioLabel = styled.label`
  display: flex;
  align-items: center;
  margin-bottom: 8px;
  cursor: pointer;
  font-family: 'Bradesco Regular Sans';
  width: max-content;
`;

export const RadioButton = styled.input<{ checked?: boolean }>`
  appearance: none;
  width: 16px;
  height: 16px;
  border-radius: 50%;
  border: 2px solid #aaa;
  margin-right: 8px;
  margin-bottom: 3px;
  cursor: pointer;

  ${({ checked }) =>
    checked
      ? css`
          background-color: #444;
          border-color: gray;
        `
      : css`
          background-color: transparent;
          border-color: #aaa;
        `}
`;
