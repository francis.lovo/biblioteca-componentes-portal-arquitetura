import React, { useState } from 'react';
import * as S from './styles';

export interface IArqRadioButtonProps {
  label?: string;
  isSelected?: boolean;
  value?: string;
  onChange?: (value: boolean) => void;
  name?: string;
}

export const ArqRadioButton = ({
  label,
  isSelected = false,
  value,
  onChange,
  name,
}: IArqRadioButtonProps) => {
  const [isChecked, setIsChecked] = useState(isSelected);

  const handleOptionChange = () => {
    if (onChange) {
      onChange(!isChecked);
    }
    setIsChecked(!isChecked);
  };
  React.useEffect(() => {
    setIsChecked(isSelected);
  }, [isSelected]);

  return (
    <S.RadioLabel>
      <S.RadioButton
        type="radio"
        name={name}
        value={value}
        checked={isChecked}
        onChange={handleOptionChange}
      />
      {label}
    </S.RadioLabel>
  );
};
