import styled, { css } from 'styled-components';

export const Card = styled.div<{ isSelected?: boolean }>`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-width: 100px;
  height: 60px;
  background-color: #ffffff;
  border-radius: 4px;
  border: 2px solid #f2f2f2;
  margin-top: 16px;
  padding: 10px;
  margin-right: 15px;
  cursor: pointer;

  ${props =>
    props.isSelected &&
    css`
      background-color: #cc092f;
      border-color: #cc092f;
    `};
`;

export const CardIcon = styled.div<{ isSelected?: boolean }>`
  margin-bottom: 8px;
  color: #cc092f;
  font-size: 30px;

  ${props =>
    props.isSelected &&
    css`
      color: #ffffff;
    `};
`;

export const CardText = styled.div<{ isSelected?: boolean }>`
  font-size: 16px;
  font-family: 'Bradesco Regular Sans';

  ${props =>
    props.isSelected &&
    css`
      color: #ffffff;
    `};
`;
