import React from 'react';
import * as S from './styles';
import { ApprovedIcons } from '../../types';

export interface IArqIconCardProps {
  icon?: ApprovedIcons;
  text?: string;
  isSelected?: boolean;
  onChange?: (value: boolean) => void;
}

export const ArqIconCard = ({
  icon,
  text = 'Text',
  isSelected = false,
  onChange,
}: IArqIconCardProps) => {
  const [selected, setSelected] = React.useState(isSelected);
  const handleClick = () => {
    if (onChange) {
      onChange(!selected);
    }
    setSelected(!selected);
  };
  React.useEffect(() => {
    setSelected(isSelected);
  }, [isSelected]);
  return (
    <S.Card isSelected={selected} onClick={handleClick}>
      {!!icon && <S.CardIcon className={icon} isSelected={selected} />}
      <S.CardText isSelected={selected}>{text}</S.CardText>
    </S.Card>
  );
};
