import React from 'react';
import * as S from './styles';

interface IChartData {
  label: string;
  value1: number;
  value2: number;
}
export interface IArqChartDualBarsProps {
  chartData?: IChartData[];
  chartYData?: number[];
  firstColor?: string;
  secColor?: string;
}

export const ArqChartDualBars = ({
  chartData = [{ label: 'X data', value1: 5, value2: 1 }],
  chartYData = [0, 1, 2, 3, 4, 5],
  firstColor = '#ff605e',
  secColor = '#6444bc',
}: IArqChartDualBarsProps) => {
  const [maior, setMaior] = React.useState(0);
  const procurarMaior = () => {
    let maiorAux = 0;
    chartYData.forEach(item => {
      if (item > maiorAux) {
        maiorAux = item;
      }
    });
    setMaior(maiorAux);
  };

  React.useEffect(() => {
    procurarMaior();
  }, []);
  return (
    <S.StyledContainerDual maior={maior < 10 ? maior * 5 : maior}>
      {chartYData.map((data, index) => {
        return (
          <S.StyledSpanYDual y={maior < 10 ? data * 5 : data} key={index}>
            {data}
          </S.StyledSpanYDual>
        );
      })}
      <S.StyledAxisYDual />
      <S.StyledRowContainerDual>
        {chartData.map((item, index) => (
          <S.StyledDualBar key={index}>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                width: '50%',
                alignItems: 'center',
              }}
            >
              <S.StyledNumberBarLabel>{item.value1}</S.StyledNumberBarLabel>
              <S.StyledBarSegment
                blue={true}
                style={{
                  height: `${
                    maior < 10 ? item.value1 * 25 : item.value1 * 5
                  }px`,
                  backgroundColor: firstColor,
                }}
              />
            </div>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                width: '50%',
                alignItems: 'center',
              }}
            >
              <S.StyledNumberBarLabel>{item.value2}</S.StyledNumberBarLabel>
              <S.StyledBarSegment
                style={{
                  height: `${
                    maior < 10 ? item.value2 * 25 : item.value1 * 5
                  }px`,
                  backgroundColor: secColor,
                }}
              />
            </div>
          </S.StyledDualBar>
        ))}
      </S.StyledRowContainerDual>
      <S.StyledlabelXContainerDual>
        {chartData.map((data, index) => {
          return (
            <S.StyledSpanXDual key={index}>{data.label}</S.StyledSpanXDual>
          );
        })}
      </S.StyledlabelXContainerDual>
    </S.StyledContainerDual>
  );
};
