import styled from 'styled-components';

export const StyledAxisYDual = styled.div`
  position: absolute;
  top: 10px;
  width: 7%;
  height: 100%;
  border-right: 1px solid #b8b8b8;
  display: flex;
  flex-direction: column;
`;
export const StyledContainerDual = styled.div<{ maior: number }>`
  position: relative;
  width: 100%;
  height: ${({ maior }) => maior * 5 + 20 + 'px'};
  font-family: Bradesco Regular Sans;
`;
export const StyledSpanYDual = styled.span<{ y: number }>`
  position: absolute;
  left: 0px;
  bottom: ${({ y }) => y * 5 + 'px'};
  font-size: 10px;
  border-bottom: 1px solid #b8b8b8;
  width: 100%;
  color: #4f4f4f;
`;
export const StyledRowContainerDual = styled.div`
  display: flex;
  flex-direction: row;
  width: 90%;
  justify-content: space-between;
  height: 100%;
  align-items: flex-end;
  margin-left: 10%;
`;
export const StyledDualBar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  width: 50px;
`;
export const StyledBarSegment = styled.div<{ blue?: boolean }>`
  width: 100%;
  transition: height 0.5s ease;
  margin-right: ${({ blue }) => (blue ? '5px' : '')};
`;
export const StyledSpanXDual = styled.div`
  font-size: 12px;
  color: #4f4f4f;
`;
export const StyledlabelXContainerDual = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  width: 90%;
  margin-left: 10%;
`;
export const StyledNumberBarLabel = styled.div`
  font-size: 10px;
  color: #bdbdbd;
`;
