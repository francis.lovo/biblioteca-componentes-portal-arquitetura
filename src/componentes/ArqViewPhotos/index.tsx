import React from 'react';
import styled from 'styled-components';
import { ArqModal } from '../ArqModal';
import { LightBlueBradesco } from '../../colors';

export interface IArqViewPhotosProps {
  isOpen?: boolean;
  onClose?: () => void;
  photos?: string[];
  titles?: string[];
  subtitles?: string[];
}

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;
const StyledPhoto = styled.img`
  width: 100%;
  height: auto;
`;
const StyledTextContainer = styled.div`
  width: 100%;
  padding-top: 15px;
  padding-bottom: 15px;
  display: flex;
  flex-direction: column;
  align-items: start;
  justify-content: center;
`;
const StyledTitle = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #4f4f4f;
  font-size: 16px;
  margin-bottom: 10px;
  margin-left: 15px;
  margin-right: 15px;
`;
const StyledSubTitle = styled.div`
  font-family: Bradesco Regular Sans;
  color: #4f4f4f;
  font-size: 14px;
  margin-left: 15px;
  margin-right: 15px;
`;
const StyledLeftArrow = styled.div`
  display: flex;
  position: absolute;
  width: 25px;
  height: 25px;
  z-index: 1000;
  border-radius: 50px;
  top: 40%;
  left: 8%;
  justify-content: center;
  align-items: center;
  background: #fff;
  cursor: pointer;
`;
const StyledRightArrow = styled.div`
  display: flex;
  position: absolute;
  width: 25px;
  height: 25px;
  z-index: 1000;
  border-radius: 50px;
  top: 40%;
  right: 8%;
  justify-content: center;
  align-items: center;
  background: #fff;
  cursor: pointer;
`;
const StyledIcon = styled.i`
  color: ${LightBlueBradesco};
  font-size: 14px;
  font-weight: 900;
`;

export const ArqViewPhotos = ({
  isOpen,
  onClose,
  photos,
  titles = ['title'],
  subtitles,
}: IArqViewPhotosProps) => {
  const [selectedPhoto, setSelectedPhoto] = React.useState(0);
  const rightPhoto = () => {
    if (photos && photos.length > 0) {
      setSelectedPhoto(
        selectedPhoto === photos.length - 1 ? 0 : selectedPhoto + 1
      );
    }
  };
  const leftPhoto = () => {
    if (photos && photos.length > 0) {
      setSelectedPhoto(
        selectedPhoto === 0 ? photos.length - 1 : selectedPhoto - 1
      );
    }
  };
  return (
    <>
      {!!isOpen && (
        <StyledLeftArrow onClick={leftPhoto}>
          <StyledIcon className="icon-icon-seta-esquerda-a" />
        </StyledLeftArrow>
      )}
      <ArqModal isOpen={isOpen} onClose={onClose} padding={0} modalWidth="70%">
        <StyledContainer>
          {photos && photos.length > 0 && (
            <StyledPhoto src={photos[selectedPhoto]} alt="InfoPhoto" />
          )}
          <StyledTextContainer>
            {titles && titles.length > 0 && titles[selectedPhoto] && (
              <StyledTitle>{titles[selectedPhoto]}</StyledTitle>
            )}
            {subtitles && subtitles.length > 0 && subtitles[selectedPhoto] && (
              <StyledSubTitle>{subtitles[selectedPhoto]}</StyledSubTitle>
            )}
          </StyledTextContainer>
        </StyledContainer>
      </ArqModal>
      {!!isOpen && (
        <StyledRightArrow onClick={rightPhoto}>
          <StyledIcon className="icon-icon-seta-direita-a" />
        </StyledRightArrow>
      )}
    </>
  );
};
