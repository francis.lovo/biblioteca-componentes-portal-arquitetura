import React from 'react';
import * as S from './styles';

export interface IArqChapterButtonIconProps {
  title?: string;
  subtitle?: string;
  link?: string;
  icon?: string;
}

export const ArqChapterButtonIcon = ({
  title = 'title',
  subtitle,
  link,
  icon,
}: IArqChapterButtonIconProps) => {
  return (
    <S.StyledChapterButton href={link}>
      <S.StyledChapterButtonIconTextContainer>
        {!!icon && <S.StyledChapterButtonImage src={icon} alt="IconInicio" />}
        <S.StyledChapterButtonTexts>
          <S.StyledChapterButtonTitle>{title}</S.StyledChapterButtonTitle>
          {!!subtitle && (
            <S.StyledChapterButtonSubtitle>
              {subtitle}
            </S.StyledChapterButtonSubtitle>
          )}
        </S.StyledChapterButtonTexts>
      </S.StyledChapterButtonIconTextContainer>
      <S.StyledChapterButtonIcon className="icon-icon-seta-direita-b" />
    </S.StyledChapterButton>
  );
};
