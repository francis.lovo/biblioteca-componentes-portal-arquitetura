import React from 'react';
import { ArqImportantLink } from '../ArqImportantLink';
import * as S from './styles';
import { ArqIconButton } from '../ArqIconButton';

export interface IArqImportantLinkListProps {
  links?: string[];
  texts?: string[];
  totalPages?: string;
  currentPage?: string;
  onClickPageUp?: () => void;
  onClickPageDown?: () => void;
  onClickLastPage?: () => void;
  onClickFirstPage?: () => void;
}

export const ArqImportantLinkList = ({
  links,
  texts = ['Important Link'],
  totalPages = '1',
  currentPage = '1',
  onClickPageUp,
  onClickPageDown,
  onClickLastPage,
  onClickFirstPage,
}: IArqImportantLinkListProps) => {
  return (
    <div>
      <S.StyledTitle>Links Importantes</S.StyledTitle>
      <S.StyledImportantLinkList>
        {texts.map((text, i) => {
          return (
            <S.StyledLink first={i < 4}>
              <ArqImportantLink
                text={text}
                href={links && links[i] ? links[i] : undefined}
              />
            </S.StyledLink>
          );
        })}
      </S.StyledImportantLinkList>
      <div
        style={{
          width: '100%',
          display: 'flex',
          justifyContent: 'flex-end',
          marginTop: '10px',
        }}
      >
        <div
          style={{
            width: '20%',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <S.StyledPageText>
            {currentPage} de {totalPages}
          </S.StyledPageText>
          <ArqIconButton
            icon="icon-icon-seta-dupla-b"
            fontSize="16px"
            onClick={onClickFirstPage}
          />
          <ArqIconButton
            icon="icon-icon-seta-esquerda-b"
            fontSize="16px"
            onClick={onClickPageUp}
          />
          <ArqIconButton
            icon="icon-icon-seta-direita-b"
            fontSize="16px"
            onClick={onClickPageDown}
          />
          <ArqIconButton
            icon="icon-icon-seta-dupla-direita-b"
            fontSize="16px"
            onClick={onClickLastPage}
          />
        </div>
      </div>
    </div>
  );
};
