import React from 'react';
import * as S from './styles';

export interface IArqBtnOptionProps {
  question?: string;
  selected?: 'sim' | 'nao';
  onChange?: (value: boolean) => void;
}

export const ArqBtnOption = ({
  question,
  selected,
  onChange,
}: IArqBtnOptionProps) => {
  const [selectedButton, setSelectedButton] = React.useState(selected);

  const handleButtonClick = (option: 'sim' | 'nao') => {
    setSelectedButton(option);
    if (onChange) {
      onChange(option === 'sim');
    }
  };

  React.useEffect(() => {
    setSelectedButton(selected);
  }, [selected]);

  return (
    <div>
      <S.Container>
        {question && <S.Question>{question}</S.Question>}
      </S.Container>
      <S.ButtonContainer>
        <S.Button
          selected={selectedButton === 'sim'}
          onClick={() => handleButtonClick('sim')}
        >
          Sim
        </S.Button>
        <S.Button
          selected={selectedButton === 'nao'}
          onClick={() => handleButtonClick('nao')}
        >
          Não
        </S.Button>
      </S.ButtonContainer>
    </div>
  );
};
