import React from 'react';
import * as S from './styles';

export interface DonutChartProps {
  data: {
    label: string;
    value: number;
    color: string;
  }[];
  radius: number;
  strokeWidth: number;
}
export const ArqDonutCharts = ({
  data,
  radius,
  strokeWidth,
}: DonutChartProps) => {
  const total = data.reduce((sum, item) => sum + item.value, 0);
  const centerX = radius;
  const centerY = radius;

  let currentAngle = -145;

  return (
    <S.Container>
      <S.SVG width={radius * 2} height={radius * 2} radius={radius}>
        <circle
          cx={centerX}
          cy={centerY}
          r={radius - strokeWidth / 2}
          fill="transparent"
          strokeWidth={strokeWidth}
        />
        {data.map(item => {
          const { label, value, color } = item;
          const percentage = (value / total) * 100;

          const startAngle = currentAngle;
          const endAngle = currentAngle + (360 * percentage) / 100;

          const startRadians = (startAngle * Math.PI) / 180;
          const endRadians = (endAngle * Math.PI) / 180;

          const startX =
            centerX + Math.cos(startRadians) * (radius - strokeWidth / 2);
          const startY =
            centerY + Math.sin(startRadians) * (radius - strokeWidth / 2);

          const endX =
            centerX + Math.cos(endRadians) * (radius - strokeWidth / 2);
          const endY =
            centerY + Math.sin(endRadians) * (radius - strokeWidth / 2);

          const largeArcFlag = percentage <= 50 ? '0' : '1';

          const pathData = `M ${startX} ${startY} A ${radius -
            strokeWidth / 2} ${radius -
            strokeWidth / 2} 0 ${largeArcFlag} 1 ${endX} ${endY}`;

          currentAngle = endAngle;

          return (
            <path
              key={label}
              d={pathData}
              fill="transparent"
              stroke={color}
              strokeWidth={strokeWidth}
            />
          );
        })}
      </S.SVG>
      <S.LegendContainer>
        {data.map(item => (
          <S.LegendItem key={item.label}>
            <S.LegendColor color={item.color} />
            <span>{item.label}</span>
          </S.LegendItem>
        ))}
      </S.LegendContainer>
    </S.Container>
  );
};
