import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: flex-end;
`;

export const SVG = styled.svg<{ radius: number }>`
  width: ${({ radius }) => radius * 2}px;
  height: ${({ radius }) => radius * 2}px;
`;

export const LegendContainer = styled.div`
  margin-left: 10px;
`;

export const LegendItem = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 5px;
`;

export const LegendColor = styled.div<{ color: string }>`
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: ${({ color }) => color};
  margin-right: 5px;
`;
