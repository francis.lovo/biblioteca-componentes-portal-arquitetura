import React from 'react';
import * as S from './styles';

export interface IArqInformationCardProps {
  children?: React.ReactNode;
}

export const ArqInformationCard = ({ children }: IArqInformationCardProps) => {
  return <S.StyledCardComponent>{children}</S.StyledCardComponent>;
};
