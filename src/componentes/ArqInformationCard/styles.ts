import styled from 'styled-components';

export const StyledCardComponent = styled.div`
  border: 1px solid #d5d5d5;
  border-radius: 5px;
  padding: 15px;
`;
