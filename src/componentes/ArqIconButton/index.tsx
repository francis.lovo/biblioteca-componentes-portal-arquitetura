import React from 'react';
import * as S from './styles';
import { ApprovedIcons } from '../../types/index';

export interface IArqIconButtonProps extends Partial<S.IArqIconButtonProps> {
  icon: ApprovedIcons;
  onClick?: () => void;
}

export const ArqIconButton = ({
  icon,
  onClick,
  fontSize,
  color,
  hoverColor,
}: IArqIconButtonProps) => {
  return (
    <S.StyledIcon
      className={icon}
      onClick={onClick}
      fontSize={fontSize}
      color={color}
      hoverColor={hoverColor}
    />
  );
};
