import React from 'react';
import * as S from './styles';
import { ArqIconButton } from '../ArqIconButton';

export interface IArqModalProps extends Partial<S.IArqModalProps> {
  children?: React.ReactNode;
  onClose?: () => void;
  noCloseButton?: boolean;
}

export const ArqModal = ({
  isOpen,
  onClose,
  children,
  topLine,
  modalWidth = '60%',
  noCloseButton,
  padding = 20,
}: IArqModalProps) => {
  if (!isOpen) {
    return null;
  }
  return (
    <S.StyledModal className="modal">
      <S.StyledModalContent
        isOpen={isOpen}
        topLine={topLine}
        modalWidth={modalWidth}
        padding={padding}
      >
        {!noCloseButton && (
          <S.StyledClose onClick={onClose}>
            <ArqIconButton
              icon="icon-icon-nav-fechar"
              fontSize="20px"
              color="#aaa"
            />
          </S.StyledClose>
        )}
        {children}
      </S.StyledModalContent>
    </S.StyledModal>
  );
};
