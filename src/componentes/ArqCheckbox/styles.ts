import styled from 'styled-components';

export const StyledCheckboxContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: max-content;
  align-items: center;
  cursor: pointer;
`;
export const StyledCheckbox = styled.div`
  border: 1px solid #d5d5d5;
  border-radius: 4px;
  width: 25px;
  height: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const StyledCheckboxLabel = styled.div`
  font-family: Bradesco Regular Sans;
  color: #4f4f4f;
  margin-left: 10px;
`;
