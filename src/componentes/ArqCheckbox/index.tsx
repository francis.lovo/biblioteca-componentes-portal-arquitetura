import React from 'react';
import * as S from './styles';
import { ArqIconButton } from '../ArqIconButton';

export interface IArqCheckboxProps {
  selected?: boolean;
  onChange?: (value: boolean) => void;
}

export const ArqCheckBox = ({ selected, onChange }: IArqCheckboxProps) => {
  const [checked, setChecked] = React.useState(selected);
  const handleClickCheckbox = () => {
    if (onChange) {
      onChange(!checked);
    }
    setChecked(!checked);
  };
  React.useEffect(() => {
    setChecked(selected);
  }, [selected]);
  return (
    <S.StyledCheckboxContainer onClick={handleClickCheckbox}>
      <S.StyledCheckbox>
        {!!checked && (
          <ArqIconButton
            icon="icon-icon-nav-check"
            color="#4f4f4f"
            fontSize="16px"
          />
        )}
      </S.StyledCheckbox>
      <S.StyledCheckboxLabel>label</S.StyledCheckboxLabel>
    </S.StyledCheckboxContainer>
  );
};
