import React from 'react';
import * as S from './styles';

export interface IArqChartBarsProps {
  chartData?: number[];
  chartYData?: number[];
  chartXData?: string[];
  color?: string;
}

export const ArqChartBars = ({
  chartData = [10],
  chartYData = [0, 5, 10],
  chartXData = ['X Data'],
  color = '#ff605e',
}: IArqChartBarsProps) => {
  const [maior, setMaior] = React.useState<number>(0);
  const buscaMaior = () => {
    let maiorAux = 0;
    chartYData.forEach(data => {
      if (data > maiorAux) {
        maiorAux = data;
      }
    });
    setMaior(maiorAux);
  };
  React.useEffect(() => {
    buscaMaior();
  }, [chartYData]);
  return (
    <S.StyledContainer maior={maior < 10 ? maior * 5 : maior}>
      {chartYData.map((data, index) => {
        return (
          <S.StyledSpanY y={maior < 10 ? data * 5 : data} key={index}>
            {data}
          </S.StyledSpanY>
        );
      })}
      <S.StyledAxisY />
      <S.StyledAxisX>
        {chartData.map((data, index) => {
          return (
            <S.StyledBar
              key={index}
              data={maior < 10 ? data * 5 : data}
              color={color}
            />
          );
        })}
      </S.StyledAxisX>
      <S.StyledlabelXContainer>
        {chartXData.map((data, index) => {
          return <S.StyledSpanX key={index}>{data}</S.StyledSpanX>;
        })}
      </S.StyledlabelXContainer>
    </S.StyledContainer>
  );
};
