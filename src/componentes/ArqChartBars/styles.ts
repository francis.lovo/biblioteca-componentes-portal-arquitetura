import styled from 'styled-components';

export const StyledBar = styled.div<{ data: number; color?: string }>`
  height: ${({ data }) => data * 10 + 'px'};
  width: 10%;
  background-color: ${({ color }) => color};
  margin-left: 5px;
`;

export const StyledAxisY = styled.div`
  position: absolute;
  top: 10px;
  width: 7%;
  height: 100%;
  border-right: 1px solid #b8b8b8;
  display: flex;
  flex-direction: column;
`;
export const StyledAxisX = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  height: 100%;
  padding: 0 7%;
`;
export const StyledContainer = styled.div<{ maior: number }>`
  position: relative;
  width: 100%;
  height: ${({ maior }) => maior * 10 + 20 + 'px'};
  font-family: Bradesco Regular Sans;
`;
export const StyledSpanY = styled.span<{ y: number }>`
  position: absolute;
  left: 0px;
  bottom: ${({ y }) => y * 10 + 'px'};
  font-size: 10px;
  border-bottom: 1px solid #b8b8b8;
  width: 100%;
  color: #4f4f4f;
`;
export const StyledSpanX = styled.div`
  font-size: 12px;
  color: #4f4f4f;
`;
export const StyledlabelXContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  padding: 0 8%;
  margin-top: 10px;
`;
