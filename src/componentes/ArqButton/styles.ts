import styled from 'styled-components';

export interface IArqButtonProps {
  color?: string;
  hoverColor?: string;
  styleType?: 'outlined' | 'filed';
}

export const StyledButton = styled.div<IArqButtonProps>`
  background-color: ${({ color, styleType }) =>
    styleType === 'filed' ? color : 'transparent'};
  color: ${({ styleType, color }) => (styleType === 'filed' ? 'white' : color)};
  cursor: pointer;
  height: 40px;
  width: max-content;
  padding-left: 10px;
  padding-right: 10px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border: 1px solid ${({ color }) => color};
  border-radius: 5px;
  &:hover {
    border: 1px solid ${({ hoverColor }) => hoverColor};
    background-color: ${({ hoverColor }) => hoverColor};
  }
  font-family: Bradesco Regular Sans;

  min-width: 80px;
`;
