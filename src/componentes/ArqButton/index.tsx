import React from 'react';
import * as S from './styles';
import { RedBradesco } from '../../colors';

export interface IArqButtonProps extends Partial<S.IArqButtonProps> {
  text?: string;
  onClick?: () => void;
}
export const ArqButton = ({
  text = 'Button',
  onClick,
  color = RedBradesco,
  hoverColor = RedBradesco,
  styleType = 'filed',
}: IArqButtonProps) => {
  return (
    <S.StyledButton
      onClick={onClick}
      color={color}
      hoverColor={hoverColor}
      styleType={styleType}
    >
      {text}
    </S.StyledButton>
  );
};
