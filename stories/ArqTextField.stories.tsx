import React from 'react';
import { ArqTextField, IArqTextFieldProps } from '../src';
export default {
  component: ArqTextField,
  title: 'ArqTextField',
};

const Template = (args: IArqTextFieldProps) => <ArqTextField {...args} />;

export const Default = Template.bind({});

export const WithLabel = Template.bind({});
WithLabel.args = {
  label: 'Label',
  placeholder: 'placeholder',
} as IArqTextFieldProps;
