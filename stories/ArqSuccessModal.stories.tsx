import React from 'react';
import { ArqButton, ArqSuccessModal, IArqSuccessModalProps } from '../src';

export default {
  component: ArqSuccessModal,
  title: 'ArqSuccessModal',
};

const Template = (args: IArqSuccessModalProps) => {
  const [modalOpen, setModalOpen] = React.useState(false);

  const handleOpenModal = () => {
    setModalOpen(true);
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };
  return (
    <>
      <ArqButton text="Abrir Modal" onClick={handleOpenModal} />
      <ArqSuccessModal
        {...args}
        isOpen={modalOpen}
        onClose={handleCloseModal}
        onClick={handleCloseModal}
      />
    </>
  );
};

export const Default = Template.bind({});
