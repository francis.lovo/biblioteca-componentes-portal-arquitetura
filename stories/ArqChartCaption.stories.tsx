import React from 'react';
import { ArqChartCaption, IArqChartCaptionProps } from '../src';
export default {
  component: ArqChartCaption,
  title: 'ArqChartCaption',
};

const Template = (args: IArqChartCaptionProps) => <ArqChartCaption {...args} />;

export const Default = Template.bind({});
export const WithLabel = Template.bind({});
WithLabel.args = {
  color: '#6444bc',
  label: 'Informação',
} as IArqChartCaptionProps;
