import React from 'react';
import {
  ArqDonutCharts,
  DonutChartProps,
} from '../src/componentes/ArqDonutCharts';

export default {
  title: 'ArqDonutCharts',
  component: ArqDonutCharts,
};

const Template = (args: DonutChartProps) => <ArqDonutCharts {...args} />;

export const Default = Template.bind({});
Default.args = {
  data: [
    { label: 'Info 1', value: 40, color: '#cc092f' },
    { label: 'Info 3', value: 60, color: '#fa4160' },
  ],
  radius: 50,
  strokeWidth: 10,
} as DonutChartProps;
