import React from 'react';
import { ArqMenuDropdown, IArqMenuDropdownProps } from '../src';
export default {
  component: ArqMenuDropdown,
  title: 'ArqMenuDropdown',
};

const Template = (args: IArqMenuDropdownProps) => <ArqMenuDropdown {...args} />;
export const Default = Template.bind({});
Default.args = {
  items: [
    { name: 'Pagina Inicial', dropdownItems: [] },
    {
      name: 'Arquitetura Delivery',
      dropdownItems: ['Chapter', 'Chapter leader'],
    },
    {
      name: 'Arquitetura Corporativa',
      dropdownItems: ['Chapter', 'Chapter leader'],
    },
    {
      name: 'Arquitetura da Informação',
      dropdownItems: ['Chapter', 'Chapter leader'],
    },
    { name: 'Comunicados', dropdownItems: [] },
    { name: 'Trilha de conhecimento', dropdownItems: [] },
    { name: 'Social', dropdownItems: [] },
    { name: 'Eventos', dropdownItems: [] },
    { name: 'Sou Novo Aqui', dropdownItems: [] },
  ],
} as IArqMenuDropdownProps;
