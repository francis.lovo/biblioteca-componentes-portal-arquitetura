import React from 'react';
import { ArqChartDualBars, IArqChartDualBarsProps } from '../src';
export default {
  component: ArqChartDualBars,
  title: 'ArqChartDualBars',
};

const Template = (args: IArqChartDualBarsProps) => (
  <ArqChartDualBars {...args} />
);

export const Default = Template.bind({});
export const AllBars = Template.bind({});
AllBars.args = {
  chartData: [
    { label: 'Barra 1', value1: 0, value2: 1 },
    { label: 'Barra 2', value1: 0, value2: 1 },
    { label: 'Barra 3', value1: 1, value2: 2 },
  ],
  chartYData: [0, 0.5, 1, 1.5, 2],
} as IArqChartDualBarsProps;
