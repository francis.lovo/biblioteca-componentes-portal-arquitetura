import React from 'react';
import { ArqFaq, IArqFaqProps } from '../src';

export default {
  title: 'ArqFaq',
  component: ArqFaq,
};

const Template = (args: IArqFaqProps) => <ArqFaq {...args} />;

export const Default = Template.bind({});
Default.args = {
  data: [
    {
      itemNames: ['Item 1', 'Item 1.1', 'Item 1.2'],
      itemTexts: ['Texto do item 1', 'Texto item 1.1'],
    },
    {
      itemNames: ['Item 2', 'item 2.1'],
      itemTexts: ['Texto do item 2', 'Texto do item 2.1'],
    },
    {
      itemNames: [],
      itemTexts: [],
    },
    {
      itemNames: [],
      itemTexts: [],
    },
    {
      itemNames: [],
      itemTexts: [],
    },
    {
      itemNames: [],
      itemTexts: [],
    },
    {
      itemNames: [
        'Qual a atuação das frentes da arquitetura nesse canal?',
        'Quem eu devo procurar em caso de Lorem ipsum?',
        'Como inserir conteúdos no canal?',
        'Como encontrar padrões para alimentar conteúdos do canal?',
        'Como posso receber notificações sobre atualizações de determinado documento?',
      ],
      itemTexts: [
        undefined,
        undefined,
        undefined,
        'Nesse caso, vá para a página de Modelos e Templates da sua arquitetura e procure a pasta quemelhor atende seu objetivo. Depois é escolher o modelo e veja o passo a passo para a criação daquele conteúdo.',
      ],
    },
  ],
  buttons: [
    'Informação',
    'Delivery',
    'Corporativa',
    'Arquiteturas',
    'Conteúdo',
    'Documentos',
    'Canal',
  ],
} as IArqFaqProps;
