import React from 'react';
import {
  ArqButton,
  ArqModal,
  ArqNotificationCard,
  ArqTextField,
  IArqModalProps,
} from '../src';
export default {
  component: ArqModal,
  title: 'ArqModal',
};

const Template = (args: IArqModalProps) => {
  const [modalOpen, setModalOpen] = React.useState(false);

  const handleOpenModal = () => {
    setModalOpen(true);
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };
  return (
    <>
      <ArqButton text="Abrir Modal" onClick={handleOpenModal} />
      <ArqModal {...args} isOpen={modalOpen} onClose={handleCloseModal} />
    </>
  );
};

export const Default = Template.bind({});
export const Notific = Template.bind({});
Notific.args = {
  children: (
    <div>
      <p
        style={{
          display: 'flex',
          width: '100%',
          justifyContent: 'flex-start',
          margin: 0,
          marginBottom: 15,
          fontFamily: 'Bradesco Regular Sans',
        }}
      >
        Notificações
      </p>

      <div style={{ marginBottom: 15 }}>
        <ArqNotificationCard
          title="Lorem inpsum"
          text="Mauris suscipit elementum convallis. Nula pulvinar gravida massa, eget tempor felis placerat tempus."
        />
      </div>
      <ArqNotificationCard
        title="Lorem inpsum"
        text="Mauris suscipit elementum convallis. Nula pulvinar gravida massa, eget tempor felis placerat tempus."
      />
    </div>
  ),
} as IArqModalProps;

export const TopLine = Template.bind({});
TopLine.args = {
  children: (
    <div
      style={{
        display: 'flex',
        width: '100%',
        justifyContent: 'flex-start',
        flexDirection: 'column',
      }}
    >
      <p
        style={{
          display: 'flex',
          width: '100%',
          justifyContent: 'flex-start',
          margin: 0,
          marginBottom: 15,
          fontFamily: 'Bradesco Regular Sans',
        }}
      >
        Informações do Projeto
      </p>

      <div style={{ marginBottom: 15, width: '90%' }}>
        <ArqTextField label={'Nome: '} placeholder="Nome Sobrenome" />
      </div>
      <div style={{ marginBottom: 15, width: '90%' }}>
        <ArqTextField label={'Telefone: '} placeholder="0000-0000" />
      </div>
    </div>
  ),
  topLine: true,
} as IArqModalProps;
