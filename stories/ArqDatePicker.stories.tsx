import React from 'react';
import { IArqDatePickerProps, ArqDatePicker } from '../src';
export default {
  component: ArqDatePicker,
  title: 'ArqDatePicker',
};

const Template = (args: IArqDatePickerProps) => {
  return <ArqDatePicker {...args} />;
};

export const Default = Template.bind({});
