import React from 'react';
import { Story, Meta } from '@storybook/react';
import ArqLargeButton from '../src/componentes/ArqLargeButton';

export default {
  title: 'ArqLargeButton',
  component: ArqLargeButton,
} as Meta;

const Template: Story = (args) => <ArqLargeButton {...args} />;

export const Default = Template.bind({});
Default.args = {};
