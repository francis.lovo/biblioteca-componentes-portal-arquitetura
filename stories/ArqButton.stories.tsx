import React from 'react';
import { ArqButton, IArqButtonProps } from '../src';
export default {
  component: ArqButton,
  title: 'ArqButton',
};

const Template = (args: IArqButtonProps) => <ArqButton {...args} />;

export const Default = Template.bind({});

export const ComTextoDef = Template.bind({});
ComTextoDef.args = {
  text: 'Text',
  color: '#0E81ED',
  styleType: 'outlined',
} as IArqButtonProps;
