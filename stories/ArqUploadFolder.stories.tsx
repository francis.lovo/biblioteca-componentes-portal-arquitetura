import React from 'react';
import { ArqUploadFolder, IArqUploadFolderProps } from '../src';
export default {
  component: ArqUploadFolder,
  title: 'ArqUploadFolder',
};

const Template = (args: IArqUploadFolderProps) => {
  const [files, setFiles] = React.useState<any>();
  return <ArqUploadFolder {...args} files={files} setFiles={setFiles} />;
};

export const Default = Template.bind({});
