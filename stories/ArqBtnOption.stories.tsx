import React from 'react';
import { ArqBtnOption, IArqBtnOptionProps } from '../src';
export default {
  component: ArqBtnOption,
  title: 'ArqBtnOption',
};

const Template = (args: IArqBtnOptionProps) => <ArqBtnOption {...args} />;

export const Default = Template.bind({});

export const WithQuestion = Template.bind({});
WithQuestion.args = {
  question: 'O escopo do projeto será definido pela área de negócio?',
  selected: 'nao',
} as IArqBtnOptionProps;
