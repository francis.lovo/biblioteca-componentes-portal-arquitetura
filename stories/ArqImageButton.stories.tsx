import React from 'react';
import { ArqImageButton, IArqImageButtonProps } from '../src';
export default {
  component: ArqImageButton,
  title: 'ArqImageButton',
};

const Template = (args: IArqImageButtonProps) => {
  return (
    <div style={{ width: '400px' }}>
      <ArqImageButton {...args} />
    </div>
  );
};

export const Default = Template.bind({});
export const ComTexto = Template.bind({});
ComTexto.args = {
  title: 'Trilhas do conhecimento',
  subtitle: 'Cadastre aqui sua trilha',
  src: require('../src/assets/imagemTeste.jpg'),
  link: 'https://google.com.br',
} as IArqImageButtonProps;
