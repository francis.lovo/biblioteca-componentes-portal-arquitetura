import React from 'react';
import { ArqRadioButton, IArqRadioButtonProps } from '../src';

export default {
  component: ArqRadioButton,
  title: 'ArqRadioButton',
};

const Template = (args: IArqRadioButtonProps) => <ArqRadioButton {...args} />;

export const Default = Template.bind({});
export const WithLabel = Template.bind({});
WithLabel.args = {
  label: 'Tempo Projeto',
  isSelected: true,
} as IArqRadioButtonProps;
