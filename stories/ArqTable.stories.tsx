import React from 'react';
import { ArqTable, IArqTableProps } from '../src';
export default {
  component: ArqTable,
  title: 'ArqTable',
};

const Template = (args: IArqTableProps) => <ArqTable {...args} />;

export const Default = Template.bind({});
export const ModalTable = Template.bind({});
ModalTable.args = {
  tableItems: [
    [
      'Bruno Santana',
      'M000000',
      'Governança Arquitetura',
      'Tipo 1',
      '60',
      'Dados',
      '00/00/00',
      '00/00/00',
      'Curso Teste',
      'Concluído',
    ],
    [
      'Bruno Santana',
      'M000000',
      'Governança Arquitetura',
      'Tipo 1',
      '60',
      'Dados',
      '00/00/00',
      '00/00/00',
      'Curso Teste',
      'Concluído',
    ],
    [
      'Bruno Santana',
      'M000000',
      'Governança Arquitetura',
      'Tipo 1',
      '60',
      'Dados',
      '00/00/00',
      '00/00/00',
      'Curso Teste',
      'Concluído',
    ],
  ],
  topLabels: [
    'Nome',
    'Chave',
    'Área',
    'Tipo de Treinamento',
    'Quantidade de Horas',
    'Trilha',
    'Data de Início',
    'Data de Término',
    'Nome',
    'Status',
  ],
} as IArqTableProps;
