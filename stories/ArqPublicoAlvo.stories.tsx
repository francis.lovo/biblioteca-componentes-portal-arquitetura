import React from 'react';
import { ArqIconCard, IArqIconCardProps } from '../src';

export default {
  component: ArqIconCard,
  title: 'ArqIconCard',
};

const Template = (args: IArqIconCardProps) => <ArqIconCard {...args} />;

export const Default = Template.bind({});
export const Clients = Template.bind({});
Clients.args = {
  text: 'Clientes',
  icon: 'icon-icon-dados-usuario',
  isSelected: true,
} as IArqIconCardProps;
