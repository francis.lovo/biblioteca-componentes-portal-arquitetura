import React from 'react';
import { ArqButton, ArqViewPhotos, IArqViewPhotosProps } from '../src';
export default {
  component: ArqViewPhotos,
  title: 'ArqViewPhotos',
};

const Template = (args: IArqViewPhotosProps) => {
  const [modalOpen, setModalOpen] = React.useState(false);

  const handleOpenModal = () => {
    setModalOpen(true);
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };
  return (
    <>
      <ArqButton text="Abrir Modal" onClick={handleOpenModal} />
      <ArqViewPhotos {...args} isOpen={modalOpen} onClose={handleCloseModal} />
    </>
  );
};

export const Default = Template.bind({});
export const WithPhotos = Template.bind({});
WithPhotos.args = {
  photos: [
    require('../src/assets/slider1.jpeg'),
    require('../src/assets/slider2.jpeg'),
    require('../src/assets/slider3.jpg'),
  ],
  subtitles: [
    'This is a beautiful city',
    'This is his favorite tree',
    'This is her house, its expensive',
  ],
  titles: ['City', 'Tree', 'House'],
} as IArqViewPhotosProps;
