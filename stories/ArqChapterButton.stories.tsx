import React from 'react';
import { ArqChapterButton, IArqChapterButtonProps } from '../src';
export default {
  component: ArqChapterButton,
  title: 'ArqChapterButton',
};

const Template = (args: IArqChapterButtonProps) => (
  <ArqChapterButton {...args} />
);

export const Default = Template.bind({});
export const Onboarding = Template.bind({});
Onboarding.args = {
  titles: ['L-mail de Onboarding'],
  subtitles: ['Questionários de impacto da Arquitetura'],
  color: 'blue',
} as IArqChapterButtonProps;

export const Opened = Template.bind({});
Opened.args = {
  titles: [
    'L-mail de Onboarding',
    'PROCESSOS E INDICADORES',
    'MODELAGEM DE DADOS',
  ],
  subtitles: [
    'Questionários de impacto da Arquitetura',
    'Neque podna quis dolorem ipsum seda',
  ],
  color: '#8b008b',
  open: true,
  links: [undefined, undefined, 'https://google.com.br'],
} as IArqChapterButtonProps;
