import React from 'react';
import { ArqSwitch, IArqSwitchProps } from '../src';
export default {
  component: ArqSwitch,
  title: 'ArqSwitch',
};

const Template = (args: IArqSwitchProps) => <ArqSwitch {...args} />;

export const Default = Template.bind({});
