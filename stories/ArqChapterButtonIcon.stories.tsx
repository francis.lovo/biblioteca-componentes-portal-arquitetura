import React from 'react';
import { ArqChapterButtonIcon, IArqChapterButtonIconProps } from '../src';
export default {
  component: ArqChapterButtonIcon,
  title: 'ArqChapterButtonIcon',
};

const Template = (args: IArqChapterButtonIconProps) => (
  <div style={{ width: '400px' }}>
    <ArqChapterButtonIcon {...args} />
  </div>
);

export const Default = Template.bind({});
export const Arquitetura = Template.bind({});
Arquitetura.args = {
  title: 'Arquitetura Delivery',
  subtitle: 'Lorem ipsum dolor sit arme',
  link: 'https://google.com.br',
  icon: require('../src/assets/computadorIcone.png'),
} as IArqChapterButtonIconProps;
