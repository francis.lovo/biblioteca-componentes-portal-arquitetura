import React from 'react';
import { ArqIconButton, IArqIconButtonProps } from '../src';
export default {
  component: ArqIconButton,
  title: 'ArqIconButton',
};

const Template = (args: IArqIconButtonProps) => <ArqIconButton {...args} />;

export const Default = Template.bind({});
Default.args = {
  icon: 'icon-icon-nav-notificacao',
} as IArqIconButtonProps;
export const Hover = Template.bind({});
Hover.args = {
  icon: 'icon-icon-nav-notificacao',
  color: '#d33553',
  hoverColor: '#98212f',
  fontSize: '60px',
} as IArqIconButtonProps;
