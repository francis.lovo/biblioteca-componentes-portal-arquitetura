import React from 'react';
import { ArqSelectInput, IArqSelectInputProps } from '../src';

export default {
  component: ArqSelectInput,
  title: 'ArqSelectInput',
};

const Template = (args: IArqSelectInputProps) => <ArqSelectInput {...args} />;

export const Default = Template.bind({});
Default.args = {
  options: [
    { value: 1, label: 'op1' },
    { value: 2, label: 'op2' },
    { value: 3, label: 'op3', disabled: true },
  ],
  label: 'label',
} as IArqSelectInputProps;
