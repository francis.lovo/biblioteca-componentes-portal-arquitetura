import React from 'react';
import { ArqProgressBar, IArqProgressBarProps } from '../src';
export default {
  component: ArqProgressBar,
  title: 'ArqProgressBar',
};

const Template = (args: IArqProgressBarProps) => <ArqProgressBar {...args} />;

export const Default = Template.bind({});
export const AllColors = Template.bind({});
AllColors.args = {
  porcentBlue: 10,
  porcentGreen: 50,
  porcentYellow: 30,
  porcentRed: 10,
} as IArqProgressBarProps;
