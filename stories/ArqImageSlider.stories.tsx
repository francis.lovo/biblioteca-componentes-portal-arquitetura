import React from 'react';
import { ArqImageSlider, IArqImageSliderProps } from '../src';
export default {
  component: ArqImageSlider,
  title: 'ArqImageSlider',
};

const Template = (args: IArqImageSliderProps) => <ArqImageSlider {...args} />;

export const Default = Template.bind({});
Default.args = {
  images: [
    require('../src/assets/slider1.jpeg'),
    require('../src/assets/slider2.jpeg'),
    require('../src/assets/slider3.jpg'),
  ],
} as IArqImageSliderProps;
