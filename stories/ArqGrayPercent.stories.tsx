import React from 'react';
import { ArqGrayPercent, IArqGrayPercent } from '../src';
export default {
  component: ArqGrayPercent,
  title: 'ArqGrayPercent',
};

const Template = (args: IArqGrayPercent) => {
  return <ArqGrayPercent {...args} />;
};

export const Default = Template.bind({});
