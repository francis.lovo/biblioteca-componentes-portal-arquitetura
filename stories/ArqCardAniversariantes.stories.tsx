import React from 'react';
import { ArqCardAniversariantes, IArqCardAniversariantesProps } from '../src';

export default {
  title: 'ArqCardAniversariantes',
  component: ArqCardAniversariantes,
};

const Template = (args: IArqCardAniversariantesProps) => (
  <ArqCardAniversariantes {...args} />
);

export const Default = Template.bind({});
export const AllWorkers = Template.bind({});
AllWorkers.args = {
  names: ['Julia Correa', 'Carlos Felino', 'Herbert Silva'],
  dates: ['10/11', '05/01'],
  infos: ['Developer', 'Doctor'],
} as IArqCardAniversariantesProps;
