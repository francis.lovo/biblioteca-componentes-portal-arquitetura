import React from 'react';
import { ArqImportantLinkList, IArqImportantLinkListProps } from '../src';
export default {
  component: ArqImportantLinkList,
  title: 'ArqImportantLinkList',
};

const Template = (args: IArqImportantLinkListProps) => (
  <ArqImportantLinkList {...args} />
);

export const Default = Template.bind({});
export const ImportantLinks = Template.bind({});
ImportantLinks.args = {
  texts: [
    'Central do Desenvolvedor',
    'Central do Desenvolvedor',
    'Central do Desenvolvedor',
    'Central do Desenvolvedor',
    'Site Importante',
    'Site Importante',
    'Site Importante',
    'Site Importante',
    'Site Importante',
    'Site Importante',
    'Site Importante',
    'Site Importante',
    'Site Importante',
    'Site Importante',
    'Site Importante',
    'Site Importante',
  ],
  totalPages: '64',
  currentPage: '1-16',
} as IArqImportantLinkListProps;
