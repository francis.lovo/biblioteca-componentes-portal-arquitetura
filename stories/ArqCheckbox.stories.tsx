import React from 'react';
import { IArqCheckboxProps, ArqCheckBox } from '../src';
export default {
  component: ArqCheckBox,
  title: 'ArqCheckBox',
};

const Template = (args: IArqCheckboxProps) => {
  return <ArqCheckBox {...args} />;
};

export const Default = Template.bind({});
