import React from 'react';
import {ArqStepper, IArqStepperProps} from '../src/componentes/ArqStepper';

export default {
    component: ArqStepper,
    title: 'ArqStepper',
    
  };

const Template = (args: IArqStepperProps) => (
    <ArqStepper {...args} />
  );

export const Default = Template.bind({});

export const ThreeSteps= Template.bind({});
ThreeSteps.args = {numberItems: 3} as IArqStepperProps;

