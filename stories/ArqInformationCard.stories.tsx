import React from 'react';
import {
  ArqInformationCard,
  ArqSelectInput,
  ArqTextField,
  IArqInformationCardProps,
} from '../src';
export default {
  component: ArqInformationCard,
  title: 'ArqInformationCard',
};

const Template = (args: IArqInformationCardProps) => (
  <ArqInformationCard {...args} />
);

export const Default = Template.bind({});
export const teste = Template.bind({});
teste.args = {
  children: (
    <div>
      <div
        style={{
          fontFamily: 'Bradesco Regular Sans',
          fontWeight: 'bold',
          fontSize: '20px',
          color: '#4f4f4f',
        }}
      >
        Informações Pessoais
      </div>
      <div style={{ marginTop: '25px' }}>
        <ArqTextField label="Nome Completo" placeholder="Nome Completo" />
      </div>
      <div style={{ marginTop: '25px' }}>
        <ArqTextField label="E-mail" placeholder="E-mail" />
      </div>
      <div style={{ marginTop: '25px' }}>
        <ArqTextField label="Chave Bradesco" placeholder="X00000" />
      </div>
      <div style={{ marginTop: '25px' }}>
        <ArqSelectInput
          label="Área de Atuação"
          options={[
            { value: 1, label: 'Área 1' },
            { value: 2, label: 'Área 2' },
          ]}
        />
      </div>
    </div>
  ),
} as IArqInformationCardProps;
