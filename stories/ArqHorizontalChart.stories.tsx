import React from 'react';
import { ArqHorizontalChart, IArqHorizointalChartProps } from '../src';
export default {
  component: ArqHorizontalChart,
  title: 'ArqHorizontalChart',
};

const Template = (args: IArqHorizointalChartProps) => {
  return <ArqHorizontalChart {...args} />;
};

export const Default = Template.bind({});
export const WithLabel = Template.bind({});
WithLabel.args = {
  labels: ['Barra1', 'Barra2', 'Barra3', 'Barra4'],
  porcents: [50, 10, 100, 70],
} as IArqHorizointalChartProps;
