import React from 'react';

import { ArqCardBirthday, IArqCardBirthdayProps } from '../src';

export default {
  title: 'ArqCardBirthday',
  component: ArqCardBirthday,
};

const Template = (args: IArqCardBirthdayProps) => <ArqCardBirthday {...args} />;

export const Default = Template.bind({});
export const JuliaCard = Template.bind({});
JuliaCard.args = {
  name: 'Julia Correia',
  date: '11/10',
  info: 'Developer',
} as IArqCardBirthdayProps;
