import React from 'react';
import { ArqGlossary, IArqGlossaryProps } from '../src';
export default {
  component: ArqGlossary,
  title: 'ArqGlossary',
};

const Template = (args: IArqGlossaryProps) => <ArqGlossary {...args} />;

export const Default = Template.bind({});
export const WithItems = Template.bind({});
WithItems.args = {
  items: [
    'aab',
    'aa',
    'Bbb',
    'DDc',
    'Cc',
    'ccc',
    'teste',
    'teste2',
    'Babbb',
    'Ac',
  ],
} as IArqGlossaryProps;
