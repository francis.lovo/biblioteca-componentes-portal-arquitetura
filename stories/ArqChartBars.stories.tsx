import React from 'react';
import { ArqChartBars, IArqChartBarsProps } from '../src';
export default {
  component: ArqChartBars,
  title: 'ArqChartBars',
};

const Template = (args: IArqChartBarsProps) => <ArqChartBars {...args} />;

export const Default = Template.bind({});
export const FiveBars = Template.bind({});
FiveBars.args = {
  chartData: [1, 1.5, 2.5],
  chartYData: [0, 0.5, 1, 1.5, 2, 2.5, 3],
  chartXData: ['SEDT - BVP', 'SEDT - HOLDING', 'SEDT - SAÚDE'],
} as IArqChartBarsProps;
