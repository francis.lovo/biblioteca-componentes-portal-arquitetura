import React from 'react';
import { ArqNotificationCard, IArqNotificationCardProps } from '../src';
export default {
  component: ArqNotificationCard,
  title: 'ArqNotificationCard',
};

const Template = (args: IArqNotificationCardProps) => (
  <ArqNotificationCard {...args} />
);

export const Default = Template.bind({});
export const WithText = Template.bind({});
WithText.args = {
  text:
    'Mauris suscipit elementum convallis. Nula pulvinar gravida massa, eget tempor felis placerat tempus.',
  title: 'Lorem inpsum',
  footer: 'Há 3 horas',
} as IArqNotificationCardProps;
