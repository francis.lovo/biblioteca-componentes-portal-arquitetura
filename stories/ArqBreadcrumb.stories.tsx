import React from 'react';
import { ArqBreadcrumb, IArqBreadcrumbProps } from '../src';
export default {
  component: ArqBreadcrumb,
  title: 'ArqBreadcrumb',
};

const Template = (args: IArqBreadcrumbProps) => <ArqBreadcrumb {...args} />;

export const Default = Template.bind({});
export const WithItems = Template.bind({});
WithItems.args = {
  items: ['Gestão', 'Orçamentos e Contratos', 'Arquitetura Geral 2023.3'],
} as IArqBreadcrumbProps;
