import React from 'react';
import { ArqTextFieldMultiline, IArqTextFieldMultilineProps } from '../src';
export default {
  component: ArqTextFieldMultiline,
  title: 'ArqTextFieldMultiline',
};

const Template = (args: IArqTextFieldMultilineProps) => (
  <ArqTextFieldMultiline {...args} />
);

export const Default = Template.bind({});
export const WithLabel = Template.bind({});
WithLabel.args = {
  label: 'Label',
  placeholder: 'placeholder',
  rows: 10,
} as IArqTextFieldMultilineProps;
