import React from 'react';
import { ArqButtonText, IArqButtonTextProps } from '../src';
export default {
  component: ArqButtonText,
  title: 'ArqButtonText',
};

const Template = (args: IArqButtonTextProps) => <ArqButtonText {...args} />;

export const Default = Template.bind({});
export const TwoButtons = Template.bind({});
TwoButtons.args = {
  previous: true,
  next: true,
} as IArqButtonTextProps;
