import React from 'react';
import { ArqImportantLink, IArqImportantLinkProps } from '../src';
export default {
  component: ArqImportantLink,
  title: 'ArqImportantLink',
};

const Template = (args: IArqImportantLinkProps) => (
  <ArqImportantLink {...args} />
);

export const Default = Template.bind({});
