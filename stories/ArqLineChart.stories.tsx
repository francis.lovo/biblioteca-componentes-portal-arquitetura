import React from 'react';
import { ArqLineChart, IArqLineChartProps } from '../src';
export default {
  component: ArqLineChart,
  title: 'ArqLineChart',
};

const Template = (args: IArqLineChartProps) => <ArqLineChart {...args} />;

export const Default = Template.bind({});
export const WithDots = Template.bind({});
WithDots.args = {
  dots: [
    { x: 1, y: 2 },
    { x: 2, y: 3 },
    { x: 3, y: 5 },
    { x: 4, y: 4 },
    { x: 5, y: 6 },
    { x: 6, y: 8 },
    { x: 7, y: 7 },
  ],
  chartWidth: 300,
  chartHeight: 200,
} as IArqLineChartProps;
