import React from 'react';
import { ArqPhotoInfoCard, IArqPhotoInfoCardProps } from '../src';
export default {
  component: ArqPhotoInfoCard,
  title: 'ArqPhotoInfoCard',
};

const Template = (args: IArqPhotoInfoCardProps) => (
  <ArqPhotoInfoCard {...args} />
);

export const Default = Template.bind({});
export const WithText = Template.bind({});
WithText.args = {
  title: 'Gerente de Arquitetura Coorporativa',
  name: 'Salvio Talamoni',
  email: 'contato@bradescoseguros.com.br',
  photo: require('../src/assets/testeImgComponente.png'),
} as IArqPhotoInfoCardProps;
