# StoryBook

para rodar o storybook

```bash
npm run storybook
```

para acessar no navegador

```bash
http://localhost:6006
```

# Componentes

iniciar add seu componente no `src/componentes`.

exporte o componente no `src/index.tsx`.

faça o stories com `nomeComponente.stories.tsx`.

```tsx
// exemplo de componente no src/componentes/ArqButton
export interface IArqButtonProps {
  text?: string;
  onClick?: () => void;
}

export const ArqButton = ({ text = 'Button', onClick }: IArqButtonProps) => {
  return <button onClick={onClick}>{text}</button>;
};
```

> nos parametros da arrow function: `({ text = 'Button', onClick }: IArqButtonProps)`, colocamos um `text = 'Button'` para definir um valor default para nosso parametro text (não obrigatório, para não quebrar caso o dev nao mande esse parametro). Caso não seja passado nenhum, quando o componente for usado, o padrão sera `'Button'`.

```tsx
// exemplo de default
const Template = (args: IArqButtonProps) => <ArqButton {...args} />;

export const Default = Template.bind({});
export const NaoDefault = Template.bind({});
NaoDefault.args = {
  text: 'text',
} as IArqButtonProps;
```

> nos stories, temos o `Default`, nele é um padrão que os parametros não obrigatórios do componente não estejam inicializados, para que no storybook mostre o componete realmente default.

> caso precise mostrar algum componente não obrigatório inicializado, pode ser feita outra variável, como o `NaoDefault` no exemplo.
